<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Tipo_recoleccion extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Estados";
			$data["lista"]=$this->db->query("select * from estado_recoleccion where estado_recoleccion_estado=1")->result_array();
		$this->vista('Tipo_recoleccion/index',$data);
		
	}

	public function mostrar(){

           $data=$this->db->query("select * from estado_recoleccion where estado_recoleccion_estado=1")->result_array();
           echo json_encode($data);exit();

	}


		public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'estado_recoleccion_descripcion' => $_POST["descripcion"]
			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('estado_recoleccion', $data);
			}else{
				$this->db->where('estado_recoleccion_id',$_POST["id"]);
				$estado=$this->db->update('estado_recoleccion', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

		public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'estado_recoleccion_estado' => 0
				);
			$this->db->where('estado_recoleccion_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('estado_recoleccion', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}



}