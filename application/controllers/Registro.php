<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Registro extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Inventario de Material Botánico";
//	$data[""]
		/*$data["lista"]=$this->db->query("SELECT
*
FROM
familia
INNER JOIN tribu ON tribu.familia_id = familia.familia_id
INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
INNER JOIN especie ON especie.genero_id = genero.genero_id
INNER JOIN herborizacion ON herborizacion.especie_id = especie.especie_id
INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
LEFT   JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
WHERE
herborizacion.herborizacion_estado = 1")->result_array();*/


$data["lista"]=$this->db->query("SELECT
  herborizacion.herborizacion_id as 'COD',
  especie.especie_descripcion as 'Especie',
  genero.genero_descripcion as 'Género',
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colecta(Cc)',
  herborizacion.herborizacion_numero_extraccion as 'N° de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as 'N° de herborizado',

  herborizacion.herborizacion_numero_accession as 'N° de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( distrito.descripcion, ',', provincia.descripcion, ',', departamento.descripcion ),
    ''
  ) AS 'region',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'Pais',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as 'N° de sobre',
  herborizacion.herborizacion_numero_bolsa as 'N° de bolsa',
  herborizacion.herborizacion_numero_caja as 'N° de caja/ taper',

  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
 IF
  (
    herborizacion.herborizacion_foto = '','NO',
    'SI'
  ) AS 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  INNER JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON distrito.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON provincia.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4 ) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1
    
    ")->result_array();
		$this->vista('Registro/index',$data);
		
	}
	public function nuevo()
	{
			$data["titulo"]="Nuevo Registro de Material Botánico ";
			$data["datos"]=$this->db->query("select * from clasificacion where clasificaicon_estado=1")->result_array();
		//	$data["lista"]=$this->db->query("select * from familia where familia_estado=1")->result_array();
		$this->vista('Registro/nuevo',$data);
		
	}


  public function editar($id){

    
  $data["titulo"]="Editar Registro ";
      $data["datos"]=$this->db->query("select * from clasificacion where clasificaicon_estado=1")->result_array();
      $data["id"]=$id;
    //  $data["lista"]=$this->db->query("select * from familia where familia_estado=1")->result_array();
    $this->vista('Registro/nuevo',$data);


  }


  public function mostrar_datos(){

            $datos["datos"]=$this->db->query("select * from herborizacion where herborizacion_id=".$_POST["id"])->result_array();
            $datos["clasificacion"]=$this->db->query("select * from clasificacion_herborizacion where herborizacion_id=".$_POST["id"])->result_array();
                

              echo  json_encode($datos);exit();


  }


private function random_string($length) { 
    $key = ''; 
    $keys = array_merge(range(0, 9), range('a', 'z')); 

    for ($i = 0; $i < $length; $i++) { 
     $key .= $keys[array_rand($keys)]; 
    } 

    return $key; 
} 


	public function guardar(){
                       $estado_id=null;
                    if($_POST["estado_id"]!=""){
                               $estado_id=$_POST["estado_id"];
                    }
              if($_POST["registro_id"]==""){
                                   $nombre="";
                                   $formato="";
                                   $nombre_total="";
                                   $response=array();
                					if ($_FILES['subir_foto']['name'] != null) {
                										if (($_FILES["subir_foto"]["type"] == "image/pjpeg")
                										    || ($_FILES["subir_foto"]["type"] == "image/jpeg")
                										    || ($_FILES["subir_foto"]["type"] == "image/png")
                										    || ($_FILES["subir_foto"]["type"] == "image/gif")) {

                											$nombre=$this->random_string("20"); 
                										 $formato = explode(".",$_FILES['subir_foto']['name']);
                										 $nombre_total=$nombre.".".$formato[1];
                										    if (move_uploaded_file($_FILES["subir_foto"]["tmp_name"], "public/imagenes/".$nombre.".".$formato[1])) {
                										        //more code here...
                										       // echo "images/".$_FILES['file']['name'];
                										    } else { 

                										    	$response["estado"]=0;
                										    	$response["mensaje"]="Error al subir la imagen";
                										    	echo  json_encode($response);
                										    		exit();
                										       // echo 0;
                										    }
                										} else {
                										   /// echo 0;
                											$response["estado"]=0;
                										    	$response["mensaje"]="Error el archivo no es una imagen";
                										    	   	echo  json_encode($response);
                										    		exit();
                										}
                					}

                    
                    $distrito=null;
                    $provincia=null;
                    $departamento=null;

                    $lugar_colecta="";
                    if(isset($_POST["lugar_colecta"])){
  $lugar_colecta=$_POST["lugar_colecta"];
                    }

                    if($_POST["pais_id"]==1){

 					               if( $_POST["distrito_id"]==""){
                           $distrito=null;
                         }
                        $provincia=$_POST["provincia_id"];
                       $departamento=$_POST["departamento_id"];
                    }
                   $data=array(
                     "herborizacion_codigo_coleta"=>$_POST["cod_colecta"],
                     "herborizacion_numero_extraccion"=>$_POST["cod_extracion"],
                     "herborizacion_numero_herborizado"=>$_POST["cod_herborizado"],
                     "herborizacion_numero_accession"=>$_POST["cod_accesion"],
                     "herborizacion_ecotipo_cv_sexo"=>$_POST["ecotipo"],
                     "estado_recoleccion_id"=> $estado_id,
                     "herborizacion_lugar_colecta"=>$lugar_colecta,
                     "pais_id"=>$_POST["pais_id"],
                     "id_distrito"=> $distrito,
                     "id_provincia"=>$provincia,
                     "id_departamento"=>$departamento,
                     "herborizacion_fecha"=>$_POST["fecha"],
                     "herborizacion_numero_sobre"=>$_POST["cod_sobre"],
                     "herborizacion_numero_bolsa"=>$_POST["cod_bolsa"],
                     "herborizacion_numero_caja"=>$_POST["cod_caja"],
                     "herborizacion_foto"=> $nombre_total,
                     "herborizacion_observacion"=>$_POST["observacion"],
                     "genero_id"=>$_POST["genero_id"],
                     "subtribu_id"=>$_POST["subtribu_id"],
                     "tribu_id"=>$_POST["tribu_id"],
                     "familia_id"=>$_POST["familia_id"],
                     "usu_id"=>$_SESSION["id_usuario"],
                     "herborizacion_fecha_registro"=>date("Y-m-d H:i:s"),
                     "especie_id"=>$_POST["especie_id"],
                     "congeambiente_id"=>$_POST["congeambiente_id"],
                   );
                   $this->db->insert("herborizacion",$data);
                   $ultimoId = $this->db->insert_id();
                   if(isset($_POST["clasificacion"])){
                              foreach ($_POST["clasificacion"] as $key => $value) {
                                  
                                  $descripcion="";
                                  if(isset($_POST["comentario".$value])){

                                      $descripcion=$_POST["comentario".$value];
                                  }

                                 	$data=array(
                                    "clasificacion_id"=>$value,
                                    "herborizacion_id"=>$ultimoId,
                                    "clasificacion_herborizacion_descripcion"=>$descripcion
                              	  );
                                 $this->db->insert("clasificacion_herborizacion",$data);
                                }
                                 }
                                $response["estado"]=1;
								      		    	$response["mensaje"]="Se registró correctamente";
							  			    	   	echo  json_encode($response);
										    		exit();
                          }
                           else{
                                   $nombre="";
                                   $formato="";
                                   $nombre_total="";
                                   $response=array();
                                    if ($_FILES['subir_foto']['name'] != null) {
                                    if (($_FILES["subir_foto"]["type"] == "image/pjpeg")
                                        || ($_FILES["subir_foto"]["type"] == "image/jpeg")
                                        || ($_FILES["subir_foto"]["type"] == "image/png")
                                        || ($_FILES["subir_foto"]["type"] == "image/gif")) {
                                      $nombre=$this->random_string("20"); 
                                     $formato = explode(".",$_FILES['subir_foto']['name']);
                                     $nombre_total=$nombre.".".$formato[1];
                                        if (move_uploaded_file($_FILES["subir_foto"]["tmp_name"], "public/imagenes/".$nombre.".".$formato[1])) {


$this->redim("public/imagenes/".$nombre.".".$formato[1],"public/imagenes/".$nombre.".".$formato[1], 800, 800);


                                       
                                        } else { 
                                          $response["estado"]=0;
                                          $response["mensaje"]="Error el al subir la imagen";
                                          echo  json_encode($response);
                                            exit();
                                        }
                                    } else {
                                      $response["estado"]=0;
                                          $response["mensaje"]="Error el archivo no es una imagen";
                                              echo  json_encode($response);
                                            exit();
                                    }
                          }









   if(isset($_POST["lugar_colecta"])){
  $lugar_colecta=$_POST["lugar_colecta"];
                    }





                    
                    $distrito=null;
                    $provincia=null;
                    $departamento=null;

                    if($_POST["pais_id"]==1){
                      if(isset($_POST["distrito_id"])){
                      if( $_POST["distrito_id"]==""){
                           $distrito=null;
                         }
                       }else{

                         $distrito=null;
                       }
                     /// $distrito=$_POST["distrito_id"];
                          if(!isset($_POST["provincia_id"])){
                            $provincia=null;
                         }else{
                            $provincia=$_POST["provincia_id"];
                         }
                      

                          if(isset($_POST["departamento_id"])){
                      if( $_POST["departamento_id"]==""){
                           $departamento=null;
                         }else{
                            $departamento=$_POST["departamento_id"];
                         }
                       }else{

                         $departamento=null;
                       }
                     
                    }





              if( $nombre_total==""){
                   $data=array(
                     "herborizacion_codigo_coleta"=>$_POST["cod_colecta"],
         

                     "herborizacion_numero_extraccion"=>$_POST["cod_extracion"],

                     "herborizacion_numero_herborizado"=>$_POST["cod_herborizado"],
                     "herborizacion_numero_accession"=>$_POST["cod_accesion"],
                     "herborizacion_ecotipo_cv_sexo"=>$_POST["ecotipo"],
                     "estado_recoleccion_id"=>$estado_id,
                     "herborizacion_lugar_colecta"=>$lugar_colecta,
                     "pais_id"=>$_POST["pais_id"],
                     "id_distrito"=> $distrito,
                     "id_provincia"=>$provincia,
                     "id_departamento"=>$departamento,
                     "herborizacion_fecha"=>$_POST["fecha"],
                     "herborizacion_numero_sobre"=>$_POST["cod_sobre"],
                     "herborizacion_numero_bolsa"=>$_POST["cod_bolsa"],
                     "herborizacion_numero_caja"=>$_POST["cod_caja"],
                     "herborizacion_foto"=> $nombre_total,
                     "herborizacion_observacion"=>$_POST["observacion"],
                     "genero_id"=>$_POST["genero_id"],
                     "subtribu_id"=>$_POST["subtribu_id"],
                     "tribu_id"=>$_POST["tribu_id"],
                     "familia_id"=>$_POST["familia_id"]
                     ,
                     "especie_id"=>$_POST["especie_id"],
                     "congeambiente_id"=>$_POST["congeambiente_id"],

                   );

                 }else{

                   $data=array(
                     "herborizacion_codigo_coleta"=>$_POST["cod_colecta"],
         

                     "herborizacion_numero_extraccion"=>$_POST["cod_extracion"],

                     "herborizacion_numero_herborizado"=>$_POST["cod_herborizado"],
                     "herborizacion_numero_accession"=>$_POST["cod_accesion"],
                     "herborizacion_ecotipo_cv_sexo"=>$_POST["ecotipo"],
                     "estado_recoleccion_id"=>$estado_id,
                     "herborizacion_lugar_colecta"=>$_POST["lugar_colecta"],
                     "pais_id"=>$_POST["pais_id"],
                     "id_distrito"=> $distrito,
                     "id_provincia"=>$provincia,
                     "id_departamento"=>$departamento,
                     "herborizacion_fecha"=>$_POST["fecha"],
                     "herborizacion_numero_sobre"=>$_POST["cod_sobre"],
                     "herborizacion_numero_bolsa"=>$_POST["cod_bolsa"],
                     "herborizacion_numero_caja"=>$_POST["cod_caja"],
                     "herborizacion_foto"=> $nombre_total,
                     "herborizacion_observacion"=>$_POST["observacion"],
                     "genero_id"=>$_POST["genero_id"],
                     "subtribu_id"=>$_POST["subtribu_id"],
                     "tribu_id"=>$_POST["tribu_id"],
                     "familia_id"=>$_POST["familia_id"],
                      "herborizacion_foto"=> $nombre_total
                      ,
                     "especie_id"=>$_POST["especie_id"],
                     "congeambiente_id"=>$_POST["congeambiente_id"],

                   );
                 }
                      $this->db->where("herborizacion_id",$_POST["registro_id"]);
                   $r=$this->db->update("herborizacion",$data);
                 //  $this->db->insert("herborizacion",$data);
                   $ultimoId =$_POST["registro_id"];

                  $this->db->query("delete from clasificacion_herborizacion where herborizacion_id=".$ultimoId);
                   if(isset($_POST["clasificacion"])){
                              foreach ($_POST["clasificacion"] as $key => $value) {
                                 $descripcion="";
                                  if(isset($_POST["comentario".$value])){

                                      $descripcion=$_POST["comentario".$value];
                                  }
                                $data=array(
                                    "clasificacion_id"=>$value,
                                    "herborizacion_id"=>$ultimoId,
                                   "clasificacion_herborizacion_descripcion"=>$descripcion
                                );
                                $this->db->insert("clasificacion_herborizacion",$data);
                              }

                   }




                   $response["estado"]=2;
                          $response["mensaje"]="Se actualizó correctamente";
                              echo  json_encode($response);
                            exit();


              }




	}
 function redim($ruta1,$ruta2,$ancho,$alto) 

{ 

    # se obtene la dimension y tipo de imagen 

  $datos=getimagesize ($ruta1); 



    $ancho_orig = $datos[0]; # Anchura de la imagen original 
    $alto_orig = $datos[1];    # Altura de la imagen original 
    $tipo = $datos[2]; 
    if ($tipo==1){ # GIF 
      if (function_exists("imagecreatefromgif")) 
        $img = imagecreatefromgif($ruta1); 
      else 
        return false; 
    } 

    else if ($tipo==2){ # JPG 

      if (function_exists("imagecreatefromjpeg")) 

        $img = imagecreatefromjpeg($ruta1); 

      else 

        return false; 

    } 

    else if ($tipo==3){ # PNG 

      if (function_exists("imagecreatefrompng")) 

        $img = imagecreatefrompng($ruta1); 

      else 

        return false; 

    } 



    # Se calculan las nuevas dimensiones de la imagen 

    if ($ancho_orig>$alto_orig) 

    { 

      $ancho_dest=$ancho; 

      $alto_dest=($ancho_dest/$ancho_orig)*$alto_orig; 

    } 

    else 

    { 

      $alto_dest=$alto; 

      $ancho_dest=($alto_dest/$alto_orig)*$ancho_orig; 

    } 

    // imagecreatetruecolor, solo estan en G.D. 2.0.1 con PHP 4.0.6+ 

    $img2=@imagecreatetruecolor($ancho_dest,$alto_dest) or $img2=imagecreate($ancho_dest,$alto_dest); 
    // Redimensionar 

    // imagecopyresampled, solo estan en G.D. 2.0.1 con PHP 4.0.6+ 

    @imagecopyresampled($img2,$img,0,0,0,0,$ancho_dest,$alto_dest,$ancho_orig,$alto_orig) or imagecopyresized($img2,$img,0,0,0,0,$ancho_dest,$alto_dest,$ancho_orig,$alto_orig); 



    // Crear fichero nuevo, según extensión. 

    if ($tipo==1) // GIF 
    if (function_exists("imagegif")) 
      imagegif($img2, $ruta2); 
    else 
      return false; 
    if ($tipo==2) // JPG 
    if (function_exists("imagejpeg")) 
      imagejpeg($img2, $ruta2); 
    else 
      return false; 

    if ($tipo==3)  // PNG 
    if (function_exists("imagepng")) 
      imagepng($img2, $ruta2); 
    else 
      return false; 
    return true; 
  }




  public function mostrar_informacion(){
    $sql="SELECT
  herborizacion.herborizacion_id as 'COD',
  especie.especie_descripcion as 'Especie',
  genero.genero_descripcion as 'Género',
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colecta(Cc)',
  herborizacion.herborizacion_numero_extraccion as 'N° de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as 'N° de herborizado',

  herborizacion.herborizacion_numero_accession as 'N° de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( distrito.descripcion, ',', provincia.descripcion, ',', departamento.descripcion ),
    ''
  ) AS 'region',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'Pais',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as 'N° de sobre',
  herborizacion.herborizacion_numero_bolsa as 'N° de bolsa',
  herborizacion.herborizacion_numero_caja as 'N° de caja/ taper',
distrito.descripcion as 'distrito', 
provincia.descripcion 'provincia',

 departamento.descripcion as 'departamento',
  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
    herborizacion.herborizacion_foto     AS 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  INNER JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON herborizacion.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON herborizacion.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4 ) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1 and herborizacion.herborizacion_id=".$_POST["id"];

$data=$this->db->query($sql)->row_array();
echo  json_encode($data);exit();

  }

}