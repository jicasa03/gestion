
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Grafico_reporte extends BaseController {

	
 public function __construct() {
        parent::__construct();
       	
      
    }


	 public function  index(){

	$data["titulo"]="Reporte Graficos";
	//	$data["lista"]=$this->db->query("select * from usuario where usu_estado=1")->result_array();
		$this->vista('Graficoreporte/index',$data);


      }


public function reporte_especie()
{
	$sql="SELECT especie.especie_descripcion as 'name',COUNT(especie.especie_id) as 'y'
FROM
herborizacion
INNER JOIN especie ON herborizacion.especie_id = especie.especie_id
WHERE herborizacion.herborizacion_estado=1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY herborizacion.especie_id";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["y"]=(float)$value["y"];

}

echo json_encode($response);exit();
}
public function reporte_genero()
{
	$sql="SELECT
genero.genero_descripcion AS `name`,
Count(genero.genero_id) AS y
FROM
herborizacion
INNER JOIN especie ON herborizacion.especie_id = especie.especie_id
INNER JOIN genero ON especie.genero_id = genero.genero_id
WHERE
herborizacion.herborizacion_estado = 1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY
genero.genero_id
";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["y"]=(float)$value["y"];

}

echo json_encode($response);exit();
}
public function reporte_subtribu()
{
	$sql="SELECT
subtribu.subtribu_descripcion AS `name`,
Count(herborizacion.subtribu_id) AS y
FROM
herborizacion
INNER JOIN especie ON herborizacion.especie_id = especie.especie_id
INNER JOIN genero ON especie.genero_id = genero.genero_id
INNER JOIN subtribu ON genero.subtribu_id = subtribu.subtribu_id
WHERE
herborizacion.herborizacion_estado = 1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY
herborizacion.subtribu_id
";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["y"]=(float)$value["y"];

}

echo json_encode($response);exit();
}

public function reporte_tribu()
{
	$sql="SELECT
tribu.tribu_descripcion AS `name`,
Count(herborizacion.tribu_id) AS y
FROM
herborizacion
INNER JOIN especie ON herborizacion.especie_id = especie.especie_id
INNER JOIN genero ON especie.genero_id = genero.genero_id
INNER JOIN subtribu ON genero.subtribu_id = subtribu.subtribu_id
INNER JOIN tribu ON subtribu.tribu_id = tribu.tribu_id
WHERE
herborizacion.herborizacion_estado = 1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY
herborizacion.tribu_id

";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["y"]=(float)$value["y"];

}

echo json_encode($response);exit();
}


public function reporte_familia()
{
	$sql="SELECT
familia.familia_descripcion AS `name`,
Count(herborizacion.familia_id) AS y
FROM
herborizacion
INNER JOIN especie ON herborizacion.especie_id = especie.especie_id
INNER JOIN genero ON especie.genero_id = genero.genero_id
INNER JOIN subtribu ON genero.subtribu_id = subtribu.subtribu_id
INNER JOIN tribu ON subtribu.tribu_id = tribu.tribu_id
INNER JOIN familia ON tribu.familia_id = familia.familia_id
WHERE
herborizacion.herborizacion_estado = 1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY
herborizacion.familia_id


";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["y"]=(float)$value["y"];

}

echo json_encode($response);exit();
}



public function reporte_estado_check()
{
	$sql="SELECT clasificacion.clasificacion_descripcion  AS `name`,

 COUNT(clasificacion.clasificacion_id) AS y
FROM
herborizacion
INNER JOIN clasificacion_herborizacion ON clasificacion_herborizacion.herborizacion_id = herborizacion.herborizacion_id
INNER JOIN clasificacion ON clasificacion_herborizacion.clasificacion_id = clasificacion.clasificacion_id
where 
herborizacion.herborizacion_estado=1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
GROUP BY clasificacion.clasificacion_id

";
$datos= $this->db->query($sql)->result_array();
$response=array();
foreach ($datos as $key => $value) {
	# code...
	$response[$key]["name"]=$value["name"];
	$response[$key]["data"][0]=(int)$value["y"];

}

echo json_encode($response);exit();
}




}