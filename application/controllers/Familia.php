<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Familia extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Familias";
			$data["lista"]=$this->db->query("select * from familia where familia_estado=1")->result_array();
		$this->vista('Familia/index',$data);
		
	}
	public function mostrar(){
			$data=$this->db->query("select * from familia where familia_estado=1")->result_array();
          echo json_encode($data);exit();
	}


		public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'familia_descripcion' => $_POST["descripcion"]
			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('familia', $data);
			}else{
				$this->db->where('familia_id',$_POST["id"]);
				$estado=$this->db->update('familia', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

		public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'familia_estado' => 0
				);
			$this->db->where('familia_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('familia', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}



}