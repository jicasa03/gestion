<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Perfiles extends BaseController {
	public function __construct() {
		parent::__construct();


	}


	public function index()
	{
		$data["titulo"]="Lista de Perfiles";
		$data["lista"] = $this->db->query("select * from perfil where per_estado=1")->result_array();
        

          $modulos = $this->db->query("select * from modulo where mod_padre=0 order by mod_orden asc")->result_array();
		foreach ($modulos as $key => $value) {
			$mod = $this->db->query("select * from modulo  where modulo.mod_padre=".$value["mod_id"]." and modulo.mod_estado=1")->result_array();
			$modulos[$key]["lista"] = $mod;
		}
		$data["modulo"]=$modulos;
                

		$this->vista("Perfiles/index",$data);
	}

	function delete_perfil(){
		if ($this->input->is_ajax_request()){
			$data = array(
				'per_estado' => 0
			);
			$this->db->where('per_id', $_POST["id"]);
			$estado=$this->db->update('perfil', $data);
			$response["estado"]=true;
			echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}
	public function save_perfil(){
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'per_descripcion' => $_POST["perfil"],
				'observacion' => $_POST["descripcion"]
			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('perfil', $data);
			}else{
				$this->db->where('per_id',$_POST["id"]);
				$estado=$this->db->update('perfil', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

	public function guardar_permiso(){
		if ($this->input->is_ajax_request()){
              $id=$_POST["id_permiso"];

              $response=array();
               if(isset($_POST["registro_perfil"]))
               {
	              $data = array(
					'per_estado_escritura' =>1,
				
				);
					$this->db->where('per_id',$id);
					$estado=$this->db->update('perfil', $data);
               }else{

					 $data = array(
										'per_estado_escritura' =>0,
									
									);
					$this->db->where('per_id',$id);
					$estado=$this->db->update('perfil', $data);
               }
              
              $this->db->query("delete from permisos where per_perfil=".$id);
if(isset($_POST["permiso"])){

              foreach ($_POST["permiso"] as $key => $value) {
              	$data=array(
                 "per_perfil"=>$id,
                 "per_modulo"=>$value,
                 "per_nuevo"=>1,
                 "per_modificar"=>1,
                 "per_eliminar"=>1,
                 "per_ver"=>1,
                 "per_imprimir"=>1,

              	);

              	$estado=$this->db->insert('permisos', $data);
              }
}
			 $response["estado"]=true;
			 $response["mensaje"]="Se guardó correctamente";

			 echo json_encode($response);exit();

			}else{
			$this->load->view('Error/404');
		}
	}

	public function mostrar_permisos()
	{
   

     if ($this->input->is_ajax_request()){
     			$id=$_POST["id"];
     			$sql="select * from permisos where per_perfil=".$id;
     			$sql1="select * from perfil where per_id=".$id;


                 $data["permisos"]=$this->db->query($sql)->result_array();
                 $data["perfil"]=$this->db->query($sql1)->row_array();
                 echo json_encode($data);exit();

			}else{
					$this->load->view('Error/404');
				}
		
	}

}
