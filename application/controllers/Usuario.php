<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Usuario extends BaseController {
	public function __construct() {
		parent::__construct();


	}


	public function index()
	{

		$data["titulo"]="Lista de Usuarios";
		$data["lista"] = $this->db->query("SELECT *
FROM
usuario
INNER JOIN perfil ON usuario.usu_perfil = perfil.per_id
where usuario.usu_estado=1")->result_array();
		$this->vista("Usuario/index",$data);
	}
	public function nuevo()
	{
		$data["titulo"]="Nuevo Usuario";
		$data["perfiles"] = $this->db->query("select * from perfil where per_estado=1")->result_array();

		$this->vista("Usuario/nuevo",$data);
	}

	public function guardar(){

		if ($this->input->is_ajax_request()){
              

               if($_POST["id"]==""){
                     $data=array(
                   "usu_usuario"=>$_POST["usuario"],
                   "usu_clave"=>$_POST["clave"],
                   "usu_fechareg"=>date("Y-m-d"),
                   "usu_perfil"=>$_POST["perfil"],
                   "usu_nombre_completo"=>$_POST["nombre_completo"],

               );
                    $verficar=$this->db->query("select * from usuario where usu_usuario='".$_POST["usuario"]."' and usu_estado=1")->result_array();
                    
                     if(count($verficar)>0){
                         
							$response["estado"]=false;
						$response["Mensaje"]="Error ya existe el usuario";
						echo json_encode($response);exit();

                     }
                      
				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('usuario', $data);
			}else{
				 $data=array(
                   "usu_usuario"=>$_POST["usuario"],
                   "usu_clave"=>$_POST["clave"],
                 
                   "usu_perfil"=>$_POST["perfil"],
                   "usu_nombre_completo"=>$_POST["nombre_completo"],

               );
				$this->db->where('usu_id',$_POST["id"]);
				$estado=$this->db->update('usuario', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();


			}else{
			$this->load->view('Error/404');
		}
	}


	public function eliminar()
	{

		if ($this->input->is_ajax_request()){
			$data = array(
							'usu_estado' => 0
						);
			$this->db->where('usu_id', $_POST["id"]);
			$estado=$this->db->update('usuario', $data);
			$response["estado"]=true;
			echo  json_encode($response);exit();
			}else{
			$this->load->view('Error/404');
		}
		
	}

	public function editar($id){
           $data["id"]=$id;
             	$data["titulo"]="Lista de Usuarios";
		$data["perfiles"] = $this->db->query("select * from perfil where per_estado=1")->result_array();

$this->vista("Usuario/nuevo",$data);
	}
	public function mostrar()
	{
		$id=$_POST["id"];
		$response=$this->db->query("select * from usuario where usu_id=".$id)->result_array();
		echo json_encode($response);exit();
	}



public function editar_perfil()
{

$data["titulo"]="Actualizar Perfil";
	//	$data["lista"] = $this->db->query("select * from perfil where per_estado=1")->result_array();
		$this->vista("Usuario/editar",$data);

}

	public function actualizar_perfil()
	{
            $nombre="";
                                   $formato="";
                                   $nombre_total="";
                                   $response=array();
                                    if ($_FILES['subir_foto']['name'] != null) {
                                    if (($_FILES["subir_foto"]["type"] == "image/pjpeg")
                                        || ($_FILES["subir_foto"]["type"] == "image/jpeg")
                                        || ($_FILES["subir_foto"]["type"] == "image/png")
                                        || ($_FILES["subir_foto"]["type"] == "image/gif")) {
                                      $nombre=$this->random_string("20"); 
                                     $formato = explode(".",$_FILES['subir_foto']['name']);
                                     $nombre_total=$nombre.".".$formato[1];
                                        if (move_uploaded_file($_FILES["subir_foto"]["tmp_name"], "public/imagenes/".$nombre.".".$formato[1])) {
                                       
                                        } else { 
                                          $response["estado"]=0;
                                          $response["mensaje"]="Error al subir la imagen";
                                          echo  json_encode($response);
                                            exit();
                                        }
                                    } else {
                                      $response["estado"]=0;
                                          $response["mensaje"]="Error el archivo no es una imagen";
                                              echo  json_encode($response);
                                            exit();
                                    }
                          }

               if( $nombre_total==""){

 				$data=array(
              
                   "usu_clave"=>$_POST["clave"],
                   "usu_nombre_completo"=>$_POST["nombre_completo"],

               );
 			}else{
					$data=array(
				              
				                   "usu_clave"=>$_POST["clave"],
				                 
				                   "usu_foto"=>$nombre_total,
				                   "usu_nombre_completo"=>$_POST["nombre_completo"],

				               );
                  $_SESSION["foto"]=$nombre_total;

 			}
				$this->db->where('usu_id',$_POST["id"]);
				$estado=$this->db->update('usuario', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";

                echo json_encode($response);exit();


	}

	
private function random_string($length) { 
    $key = ''; 
    $keys = array_merge(range(0, 9), range('a', 'z')); 

    for ($i = 0; $i < $length; $i++) { 
     $key .= $keys[array_rand($keys)]; 
    } 

    return $key; 
} 


}