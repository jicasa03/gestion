
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/third_party/vendor/autoload.php';
require_once "BaseController.php";
class Reporte_herborizacion extends BaseController {


	 public function  index(){

	$data["titulo"]="Reporte Detallado";
		//	$data["lista"]=$this->db->query("select * from familia where familia_estado=1")->result_array();
		$this->vista('Reporte_herborizacion/index',$data);


      }

      public function generar_pdf()
      {


$mpdf = new \Mpdf\Mpdf();
               $sql="SELECT
  herborizacion.herborizacion_id as 'N°',
  especie.especie_descripcion as 'Especie',
  genero.genero_descripcion as 'Género',
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colectav(Cc)',
  herborizacion.herborizacion_numero_extraccion as '# de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as '# de herborizado',

  herborizacion.herborizacion_numero_accession as '# de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( distrito.descripcion, ',', provincia.descripcion, ',', departamento.descripcion ),
    ''
  ) AS 'region',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'Pais',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as '# de sobre',
  herborizacion.herborizacion_numero_bolsa as '# de bolsa',
  herborizacion.herborizacion_numero_caja as '# de caja/ taper',

  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
  herborizacion.herborizacion_foto as 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  LEFT JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  LEFT JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  LEFT JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON herborizacion.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON herborizacion.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4 ) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1
and herborizacion.herborizacion_fecha BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'

";
if((int)$_POST["reporte"]>=5){
    if($_POST["reporte"]=="5"){
                 
        $sql.=" and herborizacion.herborizacion_id IN (SELECT  clasificacion_herborizacion.herborizacion_id from
clasificacion_herborizacion
where 
clasificacion_herborizacion.clasificacion_id=1)";
    }
      if($_POST["reporte"]=="6"){
                 
        $sql.=" and herborizacion.herborizacion_id IN (SELECT  clasificacion_herborizacion.herborizacion_id from
clasificacion_herborizacion
where 
clasificacion_herborizacion.clasificacion_id=2)";
    }
      if($_POST["reporte"]=="7"){
                 
        $sql.=" and herborizacion.herborizacion_id IN (SELECT  clasificacion_herborizacion.herborizacion_id from
clasificacion_herborizacion
where 
clasificacion_herborizacion.clasificacion_id=3)";
    }
      if($_POST["reporte"]=="8"){
                 
        $sql.=" and herborizacion.herborizacion_id IN (SELECT  clasificacion_herborizacion.herborizacion_id from
clasificacion_herborizacion
where 
clasificacion_herborizacion.clasificacion_id=4)";
    }
}

//echo $sql;exit();
$data=$this->db->query($sql)->result_array();
  
  //print_r($data);        
$html="";


if($_POST["reporte"]=="1"){
$html = $this->load->view('Pdf/R_genero',compact("data"),true);
}
if($_POST["reporte"]=="2"){

$html = $this->load->view('Pdf/R_especie',compact("data"),true);
}
if($_POST["reporte"]=="3"){

$html = $this->load->view('Pdf/R_lugar',compact("data"),true);
}
if($_POST["reporte"]=="4"){

$html = $this->load->view('Pdf/R_estado',compact("data"),true);
}



if($_POST["reporte"]=="5"){

$html = $this->load->view('Pdf/R_check',compact("data"),true);
}
if($_POST["reporte"]=="6"){

$html = $this->load->view('Pdf/R_fecha',compact("data"),true);
}
       
       if($_POST["reporte"]=="7"){

$html = $this->load->view('Pdf/R_adn',compact("data"),true);
}
     if($_POST["reporte"]=="8"){

$html = $this->load->view('Pdf/R_herborizado',compact("data"),true);
}
       //echo $html;exit();

//echo $html;exit();
//unlink('public/filename2.pdf');
        $mpdf->WriteHTML($html);


        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
// Output: video-g6swmAP8X5VG4jCi.mp4
        $nombre='documento-'.substr(str_shuffle($permitted_chars), 0, 16).'.pdf';
       $mpdf->Output('public/'.$nombre,'F'); 



      $data=array();
      $data["estado"]=true;
      $data["pdf"]="public/".$nombre;

      echo json_encode($data);exit();









      }


      public function generar_pdf_herborizacion($id){


$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
'orientation' => 'L'
]);
               $sql="SELECT
  herborizacion.herborizacion_id as 'COD',
  especie.especie_descripcion as 'Especie',
  genero.genero_descripcion as 'Género',
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colecta(Cc)',
  herborizacion.herborizacion_numero_extraccion as 'N° de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as 'N° de herborizado',

  herborizacion.herborizacion_numero_accession as 'N° de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( distrito.descripcion, ',', provincia.descripcion, ',', departamento.descripcion ),
    ''
  ) AS 'region',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'Pais',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as 'N° de sobre',
  herborizacion.herborizacion_numero_bolsa as 'N° de bolsa',
  herborizacion.herborizacion_numero_caja as 'N° de caja/ taper',
distrito.descripcion as 'distrito', 
provincia.descripcion 'provincia',

 departamento.descripcion as 'departamento',
  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN 'Si'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
    herborizacion.herborizacion_foto     AS 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  INNER JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON distrito.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON provincia.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1 and herborizacion.herborizacion_id= ".$id;

$data=$this->db->query($sql)->row_array();

        $html = $this->load->view('Pdf/R_herborizado_datos',compact("data"),true);

      $mpdf->SetTitle('Detalle de  Material Botánico');  $stylesheet = file_get_contents('public/assets/plugins/bootstrapv3/css/bootstrap.min.css');
          $mpdf->WriteHTML($stylesheet, 1); // CSS Script goes h
        $mpdf->WriteHTML($html, 2);
      $mpdf->Output();








      }


}