<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Pais extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Países";
			$data["lista"]=$this->db->query("select * from pais where pais_estado=1")->result_array();
		$this->vista('Pais/index',$data);
		
	}

	public function mostrar()
	{

		$data=$this->db->query("select * from pais where pais_estado=1")->result_array();
		echo  json_encode($data);exit();
	}

	public function departamento(){
	$data=$this->db->query("select * from departamento where estado=1")->result_array();
		echo  json_encode($data);exit();

	}

	public function provincia(){
     $id=$_POST["id"];

	$data=$this->db->query("select * from provincia where id_departamento=".$id." and estado=1")->result_array();
		echo  json_encode($data);exit();


	}


	public function distrito(){
     $id=$_POST["id"];

	   $data=$this->db->query("select * from distrito where id_provincia=".$id." and estado=1")->result_array();
		echo  json_encode($data);exit();


	}
	public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'pais_descripcion' => $_POST["descripcion"]
			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('pais', $data);
			}else{
				$this->db->where('pais_id',$_POST["id"]);
				$estado=$this->db->update('pais', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}


	public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'pais_estado' => 0
				);
			$this->db->where('pais_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('pais', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}


	

}