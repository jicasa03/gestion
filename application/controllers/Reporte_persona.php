
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/third_party/vendor/autoload.php';
require_once "BaseController.php";
class Reporte_persona extends BaseController {



	 public function  index(){

	$data["titulo"]="Reporte Por Usuario";
		$data["lista"]=$this->db->query("select * from usuario where usu_estado=1")->result_array();
		$this->vista('Reporte_persona/index',$data);


      }


       public function generar_pdf()
      {


$mpdf = new \Mpdf\Mpdf();
               $sql="SELECT
  herborizacion.herborizacion_id as 'N°',
  especie.especie_descripcion as 'Especie',
  genero.genero_descripcion as 'Género',
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colectav(Cc)',
  herborizacion.herborizacion_numero_extraccion as '# de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as '# de herborizado',

  herborizacion.herborizacion_numero_accession as '# de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( distrito.descripcion, ',', provincia.descripcion, ',', departamento.descripcion ),
    ''
  ) AS 'region',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'Pais',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as '# de sobre',
  herborizacion.herborizacion_numero_bolsa as '# de bolsa',
  herborizacion.herborizacion_numero_caja as '# de caja/ taper',

  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN 'x'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN 'x'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN 'x'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN 'x'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
  herborizacion.herborizacion_foto as 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  INNER JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON herborizacion.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON herborizacion.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4 ) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1

and date(herborizacion.herborizacion_fecha_registro) BETWEEN '".$_POST["fecha_inicio"]."' and '".$_POST["fecha_final"]."'
and herborizacion.usu_id=".$_POST["usu_id"];


$data=$this->db->query($sql)->result_array();
          
$html="";


$html = $this->load->view('Pdf/R_persona',compact("data"),true);



       


       
       //echo $html;exit();
        $mpdf->WriteHTML($html);
  
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
// Output: video-g6swmAP8X5VG4jCi.mp4
        $nombre='documento-'.substr(str_shuffle($permitted_chars), 0, 16).'.pdf';
       $mpdf->Output('public/'.$nombre,'F'); 



      $data=array();
      $data["estado"]=true;
      $data["pdf"]="public/".$nombre;

      echo json_encode($data);exit();









      }

  }