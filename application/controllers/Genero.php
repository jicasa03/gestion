<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Genero extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Géneros";
			$data["lista"]=$this->db->query("SELECT *
FROM
subtribu
INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
where genero.genero_estado=1")->result_array();
				$data["subtribu"]=$this->db->query("SELECT * FROM subtribu where subtribu_estado=1")->result_array();
		$this->vista('Genero/index',$data);
		
	}

	 public function  mostrar(){

            $id=$_POST["id"];
           $data= $this->db->query("select * from genero where subtribu_id=".$id." and genero_estado=1")->result_array();
           echo json_encode($data);exit();


      }


		public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'subtribu_id' => $_POST["subtribu_id"],
				'genero_descripcion' => $_POST["descripcion"],


			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('genero', $data);
			}else{
				$this->db->where('genero_id',$_POST["id"]);
				$estado=$this->db->update('genero', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

		public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'genero_estado' => 0
				);
			$this->db->where('genero_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('genero', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}



}