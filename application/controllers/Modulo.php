<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Modulo extends BaseController {

 public function __construct() {
        parent::__construct();
       	
      
    }


    public function index()
    {
       $data["titulo"]="Lista de Módulos";
       	$data["lista"] = $this->db->query("select * from modulo where mod_estado=1")->result_array();
       $data["padres"] = $this->db->query("select * from modulo where mod_padre=0 and mod_estado=1")->result_array();
    	$this->vista("Modulo/index",$data);
    }

    public function guardar_modulo(){
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'mod_descripcion' => $_POST["modulo"],
				'mod_url' => $_POST["url"],
				'mod_padre' => $_POST["padre"],
				'mod_icono' => $_POST["icono"]
				);
			if($_POST["id"]==""){
				$estado=$this->db->insert('modulo', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se ingresó correctamente ";
			}else{
				$this->db->where('mod_id',$_POST["id"]);
				$estado=$this->db->update('modulo', $data);
				$response["estado"]=true;
				$response["mensaje"]="Se actualizó correctamente ";
			}
			echo json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}

	function update_modulo(){
		$query = $this->db->get_where('modulo', array('mod_id' => $_POST["id"]))->result();
		echo json_encode($query);
	}

	function delete_modulo(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'mod_estado' => 0
				);
			$this->db->where('mod_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('modulo', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}


}
