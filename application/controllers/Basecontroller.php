<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseController extends CI_Controller {



	
public function __construct() {
  parent::__construct();
  date_default_timezone_set("America/Lima");
  $this->load->model("Mantenimiento_m");

 if(!isset($_SESSION["id_perfil"])){

        header('Location:'.base_url()."");
        exit();

      }


}



public function vista($cuerpo,$data=array()){

  
  ///$data["datos_usuario"]=$this->Mantenimiento_m->consulta3("SELECT * FROM empleados INNER JOIN perfiles ON empleados.perfil_id = perfiles.perfil_id AND empleados.empleado_id=".$_COOKIE["usuario_id"]); 
  //$url_array=explode("/",$url);






    
  $modulos = $this->db->query("select * from modulo where mod_padre=0 order by mod_orden asc")->result_array();
		foreach ($modulos as $key => $value) {
			$mod = $this->db->query("select modulo.* from modulo inner join permisos on(modulo.mod_id=permisos.per_modulo) inner join perfil on(perfil.per_id=permisos.per_perfil) where permisos.per_perfil=". $_SESSION["id_perfil"]." and modulo.mod_padre=".$value["mod_id"]." and modulo.mod_estado=1 order by mod_orden asc")->result_array();
			$modulos[$key]["lista"] = $mod;
		}
		$data["modulos"]=$modulos;
   


 
 //   $data["titulo"]=$nombre_sede;
   // $data["logo_empresa"]='';
   /// $data["titulo_corto"]= $nombre_sede;   
   $this->load->view('Layout/header',compact("data"));
   // $this->load->view('Layout',compact("data","modulos"));
    $this->load->view($cuerpo,compact("data"));
    $this->load->view('Layout/footer',compact("data"));
 
}
}
