<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";

class Dashboard extends BaseController {

 public function __construct() {
        parent::__construct();
       	 if(!isset($_SESSION["id_perfil"])){

  header('Location:'.base_url()."");
}
      
    }


    public function mostrar_detalle_notificacion(){


      $sql="SELECT  *
FROM
perfil
INNER JOIN usuario ON usuario.usu_perfil = perfil.per_id
INNER JOIN agenda ON agenda.usu_id = usuario.usu_id where agenda_id=".$_POST["id"];

$data=$this->db->query($sql)->row_array();


echo json_encode($data); exit();
    }


    public function index()
    {

       $sql1="select * from perfil where per_id=".$_SESSION["id_perfil"];
 $perfiles=$this->db->query($sql1)->row_array();
   // print_r($perfiles);exit();
    $data["perfil"]=$perfiles;

       $data["titulo"]="";
     //  $data["carousel"]=$this->db->query("select * from carousel where carousel_estado=1")->result_array();
       $data["total_numero"]=count($this->db->query("select * from herborizacion where herborizacion.herborizacion_estado=1")->result_array());
       $data["total_numero_mes"]=count($this->db->query("select * from herborizacion where herborizacion.herborizacion_estado=1
and MONTH(herborizacion.herborizacion_fecha_registro)=".date("m")." and 
YEAR(herborizacion.herborizacion_fecha_registro)=".date("Y"))->result_array());


       $data["usuario"]=count($this->db->query("SELECT * from usuario where usu_estado=1")->result_array());
       $data["usuario_fuera"]=$this->db->query("SELECT
  count( agenda.agenda_id ) as 'contador'
FROM
  usuario
  INNER JOIN agenda ON agenda.usu_id = usuario.usu_id 
WHERE
  agenda.agenda_fecha = '".date("Y-m-d")."' 
  AND usuario.usu_id NOT IN ( SELECT registro_notificacion.usu_id FROM registro_notificacion WHERE registro_notificacion.usu_id = ".$_SESSION["id_usuario"]." AND registro_notificacion.registro_notificacion_fecha = '".date("Y-m-d")."'  )")->result_array();


    	$this->vista("Dashboard/index",$data);
    }

    public function guardar_agenda(){

    	$data=array(
          "usu_id"=>$_SESSION["id_usuario"],
          "agenda_descripcion"=>$_POST["descripcion"],
          "agenda_fecha"=>$_POST["fecha"],
          "agenda_lugar"=>$_POST["lugar"],
          "agenda_fecha_regreso"=>$_POST["fecha_regreso"]
              	);

    	$this->db->insert("agenda",$data);
    	$response=array();
    	$response["estado"]=1;
    	$response["mensaje"]="Se guardo correctamente";
    	echo json_encode($response);
    }

    public function mostrar_notificacion(){

 
    	$data=$this->db->query("SELECT *
FROM
usuario
INNER JOIN agenda ON agenda.usu_id = usuario.usu_id
 where agenda_fecha='".date("Y-m-d")."' ")->result_array();
               echo json_encode($data);exit();
    }

    public function contar_notificacion(){
 $sql="SELECT
  count( agenda.agenda_id ) as 'contador'
FROM
  usuario
  INNER JOIN agenda ON agenda.usu_id = usuario.usu_id 
WHERE
  agenda.agenda_fecha = '".date("Y-m-d")."' 
  AND usuario.usu_id NOT IN ( SELECT registro_notificacion.usu_id FROM registro_notificacion WHERE registro_notificacion.usu_id = ".$_SESSION["id_usuario"]." AND registro_notificacion.registro_notificacion_fecha = '".date("Y-m-d")."'  )";
  
    	$data=$this->db->query($sql)->row_array();

    	echo json_encode($data);exit();
    }
     public function eliminar_noficacion(){
        $data=array(
              "registro_notificacion_fecha"=>date("Y-m-d"),
              "usu_id"=>$_SESSION["id_usuario"],


        );

        $estado=$this->db->insert("registro_notificacion",$data);

       

     }

     public function mostrardatos(){

     $data= $this->db->query("SELECT * from 
usuario where usuario.usu_id NOT IN(
SELECT usuario.usu_id
FROM
usuario
INNER JOIN agenda ON agenda.usu_id = usuario.usu_id
 where agenda_fecha='".date("Y-m-d")."' and
 usuario.usu_estado=1
) and  usuario.usu_estado=1")->result_array();

      echo json_encode($data);exit();

     }

}