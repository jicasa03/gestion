<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Inventario extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Inventario de Material Botánico";
//	$data[""]
		/*$data["lista"]=$this->db->query("SELECT
*
FROM
familia
INNER JOIN tribu ON tribu.familia_id = familia.familia_id
INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
INNER JOIN especie ON especie.genero_id = genero.genero_id
INNER JOIN herborizacion ON herborizacion.especie_id = especie.especie_id
INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
LEFT   JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
WHERE
herborizacion.herborizacion_estado = 1")->result_array();*/


$data["lista"]=$this->db->query("SELECT
  herborizacion.herborizacion_id as 'COD',
  genero.genero_descripcion as 'Género',
  especie.especie_descripcion as 'Especie',
  
  subtribu.subtribu_descripcion as 'Sub Tribu',
  tribu.tribu_descripcion as 'Tribu',
  familia.familia_descripcion as 'Familia',
  herborizacion.herborizacion_codigo_coleta as 'Código de colecta(Cc)',
  herborizacion.herborizacion_numero_extraccion as 'N° de extracción (Cl)',
  herborizacion.herborizacion_numero_herborizado as 'N° de herborizado',

  herborizacion.herborizacion_numero_accession as 'N° de accesión',
  herborizacion.herborizacion_ecotipo_cv_sexo as 'Ecotipo/ CV/ sexo',
  estado_recoleccion.estado_recoleccion_descripcion as 'Estado',
  congeambiente.congeambiente_descripcion as 'Congelación/ Ambiente',
  herborizacion.herborizacion_lugar_colecta as 'Lugar de colecta',
IF
  (
    herborizacion.pais_id = 1,
    CONCAT( provincia.descripcion),
    ''
  ) AS 'Región',
IF
  ( herborizacion.pais_id = 1, CONCAT( departamento.descripcion ), '' ) AS 'departamento',
  pais.pais_descripcion as 'País',
  herborizacion.herborizacion_fecha as 'Fecha',
  herborizacion.herborizacion_numero_sobre as 'N° de sobre',
  herborizacion.herborizacion_numero_bolsa as 'N° de bolsa',
  herborizacion.herborizacion_numero_caja as 'N° de caja/ taper',

  CASE 
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN primero.herborizacion_id  IS  NULL THEN '-'
        WHEN primero.herborizacion_id IS NOT NULL and (primero.clasificacion_herborizacion_descripcion='' or
        primero.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN primero.clasificacion_herborizacion_descripcion
    END as 'LBA-JCP/IIAP-SM Hoja seca',
    
    
    
  CASE 
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN segundo.herborizacion_id  IS  NULL THEN '-'
        WHEN segundo.herborizacion_id IS NOT NULL and (segundo.clasificacion_herborizacion_descripcion='' or
        segundo.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN segundo.clasificacion_herborizacion_descripcion
    END as 'Polvo',
    
      CASE 
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN tercer.herborizacion_id  IS  NULL THEN '-'
        WHEN tercer.herborizacion_id IS NOT NULL and (tercer.clasificacion_herborizacion_descripcion='' or
        tercer.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN tercer.clasificacion_herborizacion_descripcion
    END as 'ADN',
    
    CASE 
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NULL ) THEN '1'
        WHEN cuarto.herborizacion_id  IS  NULL THEN '-'
        WHEN cuarto.herborizacion_id IS NOT NULL and (cuarto.clasificacion_herborizacion_descripcion='' or
        cuarto.clasificacion_herborizacion_descripcion IS NOT NULL ) THEN cuarto.clasificacion_herborizacion_descripcion
    END as 'Herborizado',
    
 IF
  (
    herborizacion.herborizacion_foto = '','NO',
    'SI'
  ) AS 'Foto',
  herborizacion.herborizacion_observacion as 'Observaciones'
  
  
FROM
  familia
  INNER JOIN tribu ON tribu.familia_id = familia.familia_id
  INNER JOIN subtribu ON subtribu.tribu_id = tribu.tribu_id
  INNER JOIN genero ON genero.subtribu_id = subtribu.subtribu_id
  INNER JOIN especie ON especie.genero_id = genero.genero_id
  INNER JOIN herborizacion AS herborizacion ON herborizacion.especie_id = especie.especie_id
  INNER JOIN congeambiente ON herborizacion.congeambiente_id = congeambiente.congeambiente_id
  INNER JOIN pais ON herborizacion.pais_id = pais.pais_id
  LEFT JOIN estado_recoleccion ON herborizacion.estado_recoleccion_id = estado_recoleccion.estado_recoleccion_id
  LEFT JOIN distrito ON herborizacion.id_distrito = distrito.id_distrito
  LEFT JOIN provincia ON herborizacion.id_provincia = provincia.id_provincia
  LEFT JOIN departamento ON herborizacion.id_departamento = departamento.id_departamento
  
  LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 1 ) AS primero ON herborizacion.herborizacion_id = primero.herborizacion_id
  
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 2 ) AS segundo ON herborizacion.herborizacion_id = segundo.herborizacion_id
    
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 3 ) AS tercer ON herborizacion.herborizacion_id = tercer.herborizacion_id
    
        
    LEFT JOIN ( SELECT clasificacion_herborizacion.clasificacion_herborizacion_descripcion, clasificacion_herborizacion.herborizacion_id FROM clasificacion_herborizacion WHERE clasificacion_herborizacion.clasificacion_id = 4 ) AS cuarto ON herborizacion.herborizacion_id = cuarto.herborizacion_id
    WHERE
herborizacion.herborizacion_estado = 1

order by  herborizacion.herborizacion_id  asc
    
    ")->result_array();
		$this->vista('Registro/index',$data);
		
	}



}