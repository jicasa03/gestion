<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Tribu extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Tribus";
			$data["lista"]=$this->db->query("SELECT *
FROM
tribu
INNER JOIN familia ON tribu.familia_id = familia.familia_id
where tribu.tribu_estado=1")->result_array();
				$data["familia"]=$this->db->query("select * from familia where familia_estado=1")->result_array();
		$this->vista('Tribu/index',$data);
		
	}
      public function  mostrar(){

            $id=$_POST["id"];
           $data= $this->db->query("select * from tribu where familia_id=".$id." and tribu_estado=1")->result_array();
           echo json_encode($data);exit();


      }

		public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'familia_id' => $_POST["familia_id"],
				'tribu_descripcion' => $_POST["descripcion"],


			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('tribu', $data);
			}else{
				$this->db->where('tribu_id',$_POST["id"]);
				$estado=$this->db->update('tribu', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

		public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'tribu_estado' => 0
				);
			$this->db->where('tribu_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('tribu', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}



}