<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "BaseController.php";
class Especie extends BaseController {

	
	public function index()
	{
			$data["titulo"]="Lista de Especies";
			$data["lista"]=$this->db->query("SELECT *
FROM
genero
INNER JOIN especie ON genero.genero_id = especie.genero_id
where especie.especie_estado=1")->result_array();
				$data["genero"]=$this->db->query("SELECT * FROM genero where genero_estado=1")->result_array();
		$this->vista('Especie/index',$data);
		
	}

	 public function  mostrar(){

            $id=$_POST["id"];
           $data= $this->db->query("select * from especie where genero_id=".$id." and especie_estado=1")->result_array();
           echo json_encode($data);exit();


      }


		public function guardar()
	{
		
		if ($this->input->is_ajax_request()){

			$response=array();
			$data = array(
				'genero_id' => $_POST["genero_id"],
				'especie_descripcion' => $_POST["descripcion"],


			);
			if($_POST["id"]==""){

				$response["estado"]=true;
				$response["Mensaje"]="Se registró correctamente";
				$estado=$this->db->insert('especie', $data);
			}else{
				$this->db->where('especie_id',$_POST["id"]);
				$estado=$this->db->update('especie', $data);
				$response["estado"]=true;
				$response["Mensaje"]="Se actualizó correctamente";
			}

			echo json_encode($response);exit();

		}else{
			$this->load->view('Error/404');
		}
	}

		public function delete(){
		if ($this->input->is_ajax_request()){
			$response=array();
			$data = array(
				'especie_estado' => 0
				);
			$this->db->where('especie_id', $_POST["id"]);
			$response["estado"]=true;
			$estado=$this->db->update('especie', $data);
		echo  json_encode($response);exit();
		}else{
			$this->load->view('Error/404');
		}
	}



}