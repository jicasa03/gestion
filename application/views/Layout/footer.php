



</div>

</div>
</div>








<div id="modal_eliminar" class="modal"  tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="font-weight: bolder;">¿ESTÁS SEGURO QUE DESEA ELIMINAR?</h5>

			</div>
			<div class="modal-body">
				<input type="hidden" value="" id="id_eliminar" name="id_eliminar">
				<p style="text-align: center;">Una vez eliminado ya no se podrá recuperar</p>
			</div>
			<div class="modal-footer">
				<button type="button" id="modal_boton_eliminar" onclick="eliminar_datos()" class="btn btn-danger">Si, Acepto</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
			</div>
		</div>
	</div>
</div>







<div id="modal_notificacion" class="modal"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bolder;">DESCRIPCIÓN DE LA NOTIFICACIÓN</h5>

      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
               <div class="col-md-6">
                <h4> NOMBRE:</h4>
               </div>
                <div class="col-md-6">
                <h4 id="nombre_noti"> </h4>
               </div>
            </div>
               <div class="col-md-12">
               <div class="col-md-6">
                <h4> CARGO:</h4>
               </div>
                 <div class="col-md-6">
                <h4 id="cargo_noti"> </h4>
               </div>
            </div>
               <div class="col-md-12">
               <div class="col-md-6">
                <h4> DESCRIPCIÓN:</h4>
               </div>
                 <div class="col-md-6">
                <h4 id="descripcion_noti"> </h4>
               </div>
            </div>
               <div class="col-md-12">
               <div class="col-md-6">
                <h4> LUGAR:</h4>
               </div>
                 <div class="col-md-6">
                <h4 id="lugar_noti"> </h4>
               </div>
            </div>
               <div class="col-md-12">
               <div class="col-md-6">
                <h4> FECHAS:</h4>
               </div>
                 <div class="col-md-6">
                <h4 id="fechas_noti"> </h4>
               </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
     
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
      </div>
    </div>
  </div>
</div>

















</div>

<script type="text/javascript">
function eliminar_notificaciones(){
       
    if(valor!=0){
        $("#contador").text("0");
      $.post(base_url+"Dashboard/eliminar_noficacion",function(response){

      });
    }

}
    $(function(){

       $.post(base_url+"Dashboard/mostrar_notificacion",function(response){
    
               var html="";
               if(response.length!=0){
                             for (var i = 0; i < response.length; i++) {
                               //  response[i];

                               html+='<div onclick="mostrar_detalle_notificacion('+response[i]["agenda_id"]+')" class="notification-messages success" title="'+response[i]["agenda_descripcion"]+"("+response[i]["agenda_lugar"]+")"+'">';
         html+='<div class="iconholder">';
                 html+='<i class="icon-warning-sign"></i>';
       html+='  </div>';
        html+=' <div class="message-wrapper">';
            html+=' <div class="heading">';
                 html+=response[i]["usu_nombre_completo"];
        html+=' </div>';
       html+='  <div class="description">';
          html+=response[i]["agenda_descripcion"]+"("+response[i]["agenda_lugar"]+")";
         html+='</div>';
             html+='<div class="date pull-left">';
            html+=response[i]["agenda_fecha"];
      html+='   </div>';
             html+='</div>';
      html+='   <div class="clearfix"></div>';
     html+='</div>';
                             }
               } 

valor=0;
               $.post(base_url+"Dashboard/contar_notificacion",function(response){
                               console.log(response);
                               $("#contador").text(response["contador"]);
                               valor=parseInt(response["contador"]);
                             //  alert(valor);
               },"json");

               $("#noticacion").append(html);
       },"json");
    });



function mostrar_detalle_notificacion(id){

$.post(base_url+"Dashboard/mostrar_detalle_notificacion",{"id":id},function(response){
                 console.log(response);

                 $("#nombre_noti").text(response["usu_nombre_completo"]);
                 $("#cargo_noti").text(response["per_descripcion"]);
                $("#descripcion_noti").text(response["agenda_descripcion"]);
                 $("#lugar_noti").text(response["agenda_lugar"]);
        

                 $("#fechas_noti").text(response["agenda_fecha"]+"/"+response["agenda_fecha_regreso"]);



},"json");


$("#modal_notificacion").modal();

}























	function mostrar_notificacion_error(mensaje){

		       toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"](mensaje);
	}

	function mostrar_notificacion_success(mensaje){

		       toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["success"](mensaje);

	}
</script>



<script src="<?php echo base_url();?>public/assets/plugins/pace/pace.min.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>public/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url();?>public/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
<script src="<?php echo base_url()?>public/libreria/toast/toastr.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/webarch/js/webarch.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/js/chat.js" type="text/javascript"></script>

</body>

<!-- Mirrored from webarch.revox.io/3.0/html/blank_template.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Sep 2019 14:02:31 GMT -->
</html>
