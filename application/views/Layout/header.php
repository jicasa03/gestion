﻿<!DOCTYPE html>
<html>

<!-- Mirrored from webarch.revox.io/3.0/html/blank_template.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Sep 2019 14:02:31 GMT -->
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Sistema de Información de Gestión de Material Botánico</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/img/logo.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />

<style type="text/css">
    .admin-bar{

        z-index: 10000;
    }
</style>
<style type="text/css">
	
label,h1,h2,h3,h4,h5,h6,td,th,a,div,button,option{
font-family: Ubuntu !important;
}


.btn-danger{
	background-color: #c62828 !important;
}

.btn-primary{

	background-color: #2e7d32 !important;
}

</style>
<link href="<?php echo base_url();?>public/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?php echo base_url();?>public/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url();?>public/assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />

<!--
<link href="<?php echo base_url();?>public/assets/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />-->
<script src="<?php echo base_url();?>public/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>public/webarch/css/webarch.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>public/assets/plugins/jquery-datatable/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-datatable/extra/js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
</head>
<link href="<?php echo base_url();?>public/libreria/toast/toastr.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />




<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/fc-3.3.0/fh-3.1.6/r-2.2.3/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.20/af-2.3.4/b-1.6.1/b-colvis-1.6.1/b-flash-1.6.1/b-html5-1.6.1/b-print-1.6.1/fc-3.3.0/fh-3.1.6/r-2.2.3/datatables.min.js"></script>


<body >

<div class="header navbar navbar-inverse ">

<div class="navbar-inner">
<div class="header-seperation">
<ul class="nav pull-left notifcation-center visible-xs visible-sm">
<li class="dropdown">
<a  href="#main-menu" data-webarch="toggle-left-side">
<i class="material-icons">menu</i>
</a>
</li>
</ul>

<a href="<?php echo base_url();?>Dashboard">
<div style="margin-top: 0px;margin-bottom: 0px;" class="text-center"><img src="<?php echo base_url();?>public/img/logo2.png" class="logo" alt="" src="<?php echo base_url();?>public/img/logo2.png" src="<?php echo base_url();?>public/img/logo2.png" style="padding-bottom: 15px;text-align: center;width: 120px;height: 50px" />
	</div>
<!--<a style="color: #fff;font-family: Ubuntu;font-size:35px;"> IIAP-PIBA </a>-->
</a>

<ul class="nav pull-right notifcation-center">


 
</ul>
</div>

<div class="header-quick-nav">

<div class="pull-left">
<ul class="nav quick-section">
<li class="quicklinks">
<a href="#" class="" id="layout-condensed-toggle">
<i class="material-icons">menu</i>
</a>
</li>

<li style="margin-left: 30px;" class="quicklinks">
<a onclick="eliminar_notificaciones();" href="#" class="" id="my-task-list" data-placement="bottom" data-content="" data-toggle="dropdown" data-original-title="Notificaciones">
<i class="material-icons" style="font-size: 28px;">notifications_none</i>
<span class="badge badge-important bubble-only" style="" id="contador">0</span>
</a>
</li>
</ul>

<!--<ul class="nav quick-section">
<li class="quicklinks  m-r-10">
<a href="#" class="">
<i class="material-icons">refresh</i>
</a>
</li>
<li class="quicklinks">
<a href="#" class="">
<i class="material-icons">apps</i>
</a>
</li>
<li class="quicklinks"> <span class="h-seperate"></span></li>
<li class="quicklinks">
<a href="#" class="" id="my-task-list" data-placement="bottom" data-content='' data-toggle="dropdown" data-original-title="Notifications">
<i class="material-icons">notifications_none</i>
<span class="badge badge-important bubble-only"></span>
</a>
</li>
<li class="m-r-10 input-prepend inside search-form no-boarder">
<span class="add-on"> <i class="material-icons">search</i></span>
<input name="" type="text" class="no-boarder " placeholder="Search Dashboard" style="width:250px;">
</li>
</ul>-->
</div>


<div class="pull-right">
<div class="chat-toggler sm">
<img src="<?php echo base_url();?>public/img/logo1.png" alt="" data-src="<?php echo base_url();?>public/img/logo1.png" data-src-retina="<?php echo base_url();?>public/img/logo1.png" width="40" height="50" />
<img src="<?php echo base_url();?>public/img/LOGOTRANSPARENTE.png" alt="" data-src="<?php echo base_url();?>public/img/LOGOTRANSPARENTE.png" data-src-retina="<?php echo base_url();?>public/img/LOGOTRANSPARENTE.png" width="80" height="35" />



</div>
<ul class="nav quick-section ">
<li class="quicklinks">
<a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
<i class="material-icons">tune</i>
</a>
<ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
<li>
<a href="<?php echo base_url() ?>Usuario/editar_perfil"><i class="material-icons">settings_applications</i>&nbsp;&nbsp;Configuración</a>
</li>
<li>
<a href="<?php echo base_url() ?>Login/cerrar_sesion"><i class="material-icons">power_settings_new</i>&nbsp;&nbsp;Cerrar Sesión</a>
</li>
</ul>
</li>


</ul>
</div>

</div>

</div>

</div>




<!-- notificaciones-->
<div id="notification-list" style="display:none">
<div id="noticacion" style="width:300px">



</div>
</div>
<!-- notificaciones-->
<div class="page-container row-fluid">

<div class="page-sidebar " id="main-menu">

<div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
<div class="user-info-wrapper sm">
<div class="profile-wrapper sm">

<?php if($_SESSION["foto"]=="" || ($_SESSION["foto"]==null)){ ?>
<img src="<?php echo base_url();?>public/img/user-default.png" alt="" data-src="<?php echo base_url();?>public/img/user-default.png" data-src-retina="<?php echo base_url();?>public/img/user-default.png" width="69" height="69" />
<?php } else{ ?>

<img src="<?php echo base_url();?>public/img/user-default.png" alt="" data-src="<?php echo base_url();?>public/imagenes/<?php echo $_SESSION["foto"]; ?>" data-src-retina="<?php echo base_url();?>public/imagenes/<?php echo $_SESSION["foto"]; ?>" width="69" height="69" />
<?php } ?>





<div class="availability-bubble online"></div>
</div>
<div class="user-info sm">
<div class="username"><span style="font-size: 10px;font-family: Ubuntu;"><?php echo  $_SESSION["nombre"];?></span></div>
<div class="status" style="font-family: Ubuntu;"><?php echo  $_SESSION["perfil"];?></div>
</div>
</div>


<p class="menu-title sm">NOTIFICACIONES <span class="pull-right"></span></p>
<ul>

<li>

		<div id="cuerpo_fotos">
		
			  
		</div>
	</li>

	<hr  style="margin:0px; border:1px solid;margin-top: 10px;margin-left: 25px;margin-right:25px;">
	<p id="menu" class="menu-title sm">MÓDULOS <span class="pull-right"></span></p>	


	<?php 

	 $sql1="select * from perfil where per_id=".$_SESSION["id_perfil"];
 $perfiles=$this->db->query($sql1)->row_array();
    //print_r($perfiles);exit();
  //  $data["perfil"]=$perfiles;

//print_r($data);
$per =$perfiles;

if($per["per_estado_escritura"]=="1"){
	?>
	<li>

		<a href="<?php echo base_url(); ?>registro/nuevo"><i  class="material-icons"  >table_chart</i> <span class="title">Registro</span>
		</a>
	</li>

	<?php 
    }
   
	?>

<li>

		<a href="<?php echo base_url(); ?>Inventario"><i  class="material-icons"  >assessment</i> <span class="title">Inventario</span>
		</a>
	</li>
	<?php 
$lista_modulos =$data["modulos"];
	foreach ($lista_modulos as $value) { 

									if(count($value["lista"])>0){ ?>

									<li>


										<a href="#"><i  class="material-icons"  ><?php echo strtolower($value["mod_icono"])?></i> <span class="title"><?php echo ($value["mod_descripcion"])?></span>

 <span class=" arrow"></span>
										</a>

										<ul class="sub-menu">

											<?php foreach ($value["lista"] as $val) { ?>

											<li ><a href="<?php echo base_url().$val["mod_url"]?>" >

												<i class="<?php echo strtolower($val["mod_icono"])?>"></i><?php echo $val["mod_descripcion"]?></a></li>

											<?php } 	?>	

										</ul>

									</li>

									<?php } 

								}

								?>





</ul>


<style type="text/css">
#cuerpo_fotos{

margin-left: 30px;
margin-top: 20px;

  width: 200px;
    overflow-x: auto;
    white-space: nowrap;
   
display: block;
    position: relative;

}

#cuerpo_fotos::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;

}

#cuerpo_fotos::-webkit-scrollbar
{
	width: 1px;
	background-color: #F5F5F5;
	height: 5px;
}

#cuerpo_fotos::-webkit-scrollbar-thumb
{
	background-color: #000000;
}


.img_foto{

  border-radius: 50%;
  width: 30px;
  height: 30px;
  margin-left: 5px;

    position: relative;

}






</style>

<script type="text/javascript">






	base_url="<?php echo base_url() ?>";

	$(function (){
		//alert("<?php echo $_SESSION["foto"]; ?>");
		// body...

		cargar_usuario();
	});


	function cargar_usuario()
	{
		$.post(base_url+"Dashboard/mostrardatos",function(response){
                  console.log(response);
                  var html="";
                  if(response.length>0){
                    for (var i = 0; i < response.length; i++) {
                    //	response[i];
                    img=response[i]["usu_foto"];
                    //alert(img);
                    console.log(img);
                    if(img!=""){

                    	 html+='<img  title="'+response[i]["usu_nombre_completo"]+'" class="img_foto" src="/public/imagenes/'+response[i]["usu_foto"]+'" alt="" data-src="/public/imagenes/'+response[i]["usu_foto"]+'" data-src-retina="/public/imagenes/'+response[i]["usu_foto"]+'" >';
                    }else{
                    	
                    	 html+='<img  title="'+response[i]["usu_nombre_completo"]+'" class="img_foto" src="'+base_url+'public/img/user-default.png" alt="" data-src="'+base_url+'public/img/user-default.png" data-src-retina="'+base_url+'public/img/user-default.png" >';
                    }
                  }
                  $("#cuerpo_fotos").empty().append(html);
                    }
		},"json");
	}
</script>
	
<div class="clearfix"></div>

</div>
</div>
<a href="#" class="scrollup">Scroll</a>
<div class="footer-widget">

<div class="pull-right">

<a href="<?php echo base_url() ?>Login/cerrar_sesion"><i class="material-icons">power_settings_new</i></a></div>
</div>


<div class="page-content">

<div id="portlet-config" class="modal hide">
<div class="modal-header">
<button data-dismiss="modal" class="close" type="button"></button>
<h3>Widget Settings</h3>
</div>
<div class="modal-body"> Widget settings form goes here </div>
</div>
<div class="clearfix"></div>
<div class="content">
<div class="page-title">
<h3 style="margin-left: 15px;"><?php echo $data["titulo"]; ?></h3>
</div>
<div class="col-md-12">
