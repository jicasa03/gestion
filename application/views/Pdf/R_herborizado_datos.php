<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
	<title> </title>

</head>


<body>

<link href="<?php echo base_url();?>public/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    	<h2 style="text-align: center">Detalle de Material Botánico</h2>

<div class="container">
	<div class="col-md-12">
			  <div class="col-xs-2 col-sm-2">
                        <div class="col">
                            <div class="text-center" id="imagen">

                               <?php if($data["Foto"]!=""){ ?>

                                 <img id="image_herborizacion" src="<?php  echo base_url()?>public/imagenes/<?php echo $data["Foto"]; ?>" style="width: 110px;height: 110px;">
                                <?php } else{?>

 <img id="image_herborizacion" src="<?php  echo base_url()?>public/default.png" style="width: 110px;height: 110px;">

                                <?php }?>
                               
                            </div>
                        </div>
                        <div class="col">
                            <div class="text-center" style="background-color:#d1dade;padding-top: 1px;padding-bottom: 1px;margin-top: 5px;">
                                <h5 style="color:black">Código Referencial</h5>
                            </div>
                        </div>
                        <div class="col" style="margin-top: 10px;">
                            <table  style="border:1px solid;">
                                <tbody>
                                    <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;padding-right: 2px;"><label style="font-size: 8px;margin-left: 3px;"> Cod. Colecta</label>
                                        </td>
                                        <td id="" style="width: 100px;border:1px solid #ddd;">
                                             <label   id="cod_colecta" style="font-size: 8px;margin-left: 3px;">  <?php echo $data['Código de colecta(Cc)'] ?></label>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px ;"><label style="font-size: 8px;margin-left: 3px;">N° de Extracción </label>
                                        </td>
                                        <td id=""  style="width: 100px;border:1px solid #ddd;">
                                              <label   id="cod_extracion" style="font-size: 8px;margin-left: 3px;">      <?php echo $data['N° de accesión'] ?></label>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;"><label style="font-size: 8px;margin-left: 3px;"> N° de Herborizado</label>
                                        </td>
                                        <td id="" style="width: 100px;border:1px solid #ddd;">
                                           <label   id="cod_herborizado" style="font-size: 8px;margin-left: 3px;">        <?php echo $data['N° de herborizado'] ?></label>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;"> <label style="font-size: 8px;margin-left: 3px;">N° de accesión     </label>                                   </td>
                                        <td id="" style="width: 100px;border:1px solid #ddd;">
                                                 <label   id="cod_accesion" style="font-size: 8px;margin-left: 3px;">    <?php echo $data['N° de extracción (Cl)'] ?></label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                               <div class="col">
                            <div class="text-center" style="background-color:#d1dade;padding-top: 1px;padding-bottom: 1px;margin-top: 5px;">
                                <h5 style="color:black">Localización de Laboratorio</h5>
                            </div>
                        </div>
                        <div class="col" style="margin-top: 10px;">
                            <table style="border:1px solid  #ddd;">
                                <tbody>
                                
                                      <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;"><label style="font-size: 8px;margin-left: 3px;"> N° de sobre</label></td>
                                        <td  id="" style="width: 100px;border:1px solid #ddd;">
                                         <label   id="n_sobre" style="font-size: 8px;margin-left: 3px;"><?php echo $data['N° de caja/ taper'] ?></label>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;"><label style="font-size: 8px;margin-left: 3px;"> N° de bolsa</label></td>
                                        <td  id="" style="width: 100px;border:1px solid #ddd;">
                                         <label   id="n_sobre" style="font-size: 8px;margin-left: 3px;"><?php echo $data['N° de bolsa'] ?></label>
                                        </td>
                                    </tr>
                                      <tr style="border:1px solid #ddd;">
                                        <td style="width: 80px;border:1px solid #ddd;font-size: 8px;"><label style="font-size: 8px;margin-left: 3px;">  N° de caja/taper</label></td>
                                        <td  id="" style="width: 100px;border:1px solid #ddd;">
                                         <label   id="n_sobre" style="font-size: 8px;margin-left: 3px;"><?php echo $data['N° de sobre'] ?></label>
                                        </td>
                                    </tr>
                         
                                
                                </tbody>
                            </table>
                        </div>
                    </div>	

                      <div class="col-xs-2 col-sm-2">
                         <div class="col">
                            <div class="form-group">
                            <label>Familia</label>
                           <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Familia'])); ?></textarea>
                        </div>
                       </div>
                        <div class="col">
                            <div class="form-group">
                            <label>Tribu</label>
                             <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Tribu'])); ?></textarea>
                        </div>
                       </div>
                        <div class="col">
                            <div class="form-group">
                            <label>Subtribu</label>
                            <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Sub Tribu'])) ?></textarea>
                        </div>
                       </div>
                       <div class="col">
                            <div class="form-group">
                            <label>Género</label>
                          <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Género'])) ?></textarea>
                        </div>
                       </div>
                         <div class="col">
                            <div class="form-group">
                            <label>Especie</label>
                           <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Especie'])) ?></textarea>
                        </div>
                       </div>
                      </div>
                        <div class="col-xs-2 col-sm-2">
                        	
                        	<div class="col">
                        <div class="form-group">
                            <label>Congelado/Ambiente</label>
                           <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Congelación/ Ambiente'])) ?></textarea>
                        </div>
                      </div>
                       <div class="col">
                        <div class="form-group">
                            <label>Ecotipo/Cv/Sexo</label>
                           <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Ecotipo/ CV/ sexo'])) ?></textarea>
                        </div>
                      </div>


                       <div class="col">
                        <div class="form-group">
                            <label>Estado</label>
                            <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Estado'])) ?></textarea>
                        </div>
                      </div>
                       <div class="col">
                        <div class="form-group">
                            <label>Lugar de Colecta</label>
                            <!--<input type="type" readonly="true" value="<?php echo $data['Lugar de colecta'] ?>" name="colecta" id="colecta" class="form-control">-->

                              <textarea rows="3" style="border: 1px solid black;"  class="form-control">&nbsp;<?php echo ucfirst(strtolower($data['Lugar de colecta'])) ?></textarea>
                        </div>
                      </div>
                       <div class="col">
                        <div class="form-group">
                            <label>Fecha</label>
                           <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Fecha'])) ?></textarea>
                        </div>
                      </div>
                        </div>
                          <div class="col-xs-2 col-sm-2">
                          	
                          	<div class="col">
                            <div class="form-group">
                                <label>País</label>
                                <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Pais'])) ?></textarea>
                            </div>
                           </div>
                            <div class="col">
                            <div class="form-group">
                                <label>Departamento</label>
                               <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['departamento'])); ?></textarea>
                            </div>
                           </div>
                            <div class="col">
                            <div class="form-group">
                                <label>Provincia</label>
                             <textarea  rows="1"  class="form-control"><?php echo  ucfirst(strtolower($data['provincia'])) ?></textarea>
                            </div>
                           </div>
                           <div class="col">
                            <div class="form-group">
                                <label>Distrito</label>
                               <textarea  rows="1"  class="form-control"><?php echo  ucfirst(strtolower($data['distrito'])); ?></textarea>
                            </div>
                           </div>
                               <div class="col">
                            <div class="form-group">
                                <label>Observaciones</label>
                              <textarea rows="3" style="border: 1px solid black;"  class="form-control">&nbsp;<?php echo ucfirst(strtolower($data['Observaciones'])) ?></textarea>
                            </div>
                           </div>
                          </div>
                            <div class="col-xs-2 col-sm-2">
                            	<div class="col">
                            <div class="form-group">
                                <label>Hoja Seca</label>
                              <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['LBA-JCP/IIAP-SM Hoja seca'])) ?></textarea>
                            </div>
                        </div>
                         <div class="col">
                            <div class="form-group">
                                <label>Polvo</label>
                              <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Polvo'])) ?> </textarea>
                            </div>
                        </div>
                         <div class="col">
                            <div class="form-group">
                                <label>Extracción de ADN</label>
                             <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['ADN'])) ?></textarea>
                            </div>
                        </div>
                         <div class="col">
                            <div class="form-group">
                                <label>Herborizado</label>
                               <textarea  rows="1"  class="form-control"><?php echo ucfirst(strtolower($data['Herborizado'])) ?></textarea>
                            </div>
                        </div>
                            </div>
                              <div class="col-sm-2"></div>
	</div>
</div>
</body>
</html>