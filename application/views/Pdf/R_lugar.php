<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Reporte de Material Botánico por Lugar de Colecta</title>
</head>
<body>

     <center ><h2 style="margin-bottom: 11px;text-align: center">Reporte de Material Botánico por Lugar de Colecta </h2></center>


	<table style="border: 1px solid black;border-collapse: collapse;">
		<thead style="border: 1px solid black;">
			<tr style="border: 1px solid black;">
				<td style="font-weight: bold;border: 1px solid black;width: 120px;font-size: 12px;">CODIGO LBA-JCP </td>
				

				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">Especie </td>
				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">Género  </td>
				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">Familia </td>
				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">Estado  </td>

<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">LUGAR  </td>

				<td style="font-weight: bold;border: 1px solid black;width: 300px;font-size: 12px;">Localización de laboratorio </td>
			
				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">País </td>
				<td style="font-weight: bold;border: 1px solid black;width: 100px;font-size: 12px;">Ubicación </td>
		


                

			</tr>
		</thead>
		<tbody>
			<tr>
			      <?php
        
                      foreach ($data as $key => $value) {
                       echo "<tr>";
                     //  echo '<td><a title="Editar" onclick="editar('.$value["herborizacion_id"].')" class="btn btn-primary"><i class="fa fa-paste "></i> </a> </td>';

                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>LBA-JCP ".$value["N°"]."</td>";
                   
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Especie"]."</td>";
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Género"]."</td>";
                        
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Familia"]."</td>";
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Estado"]."</td>";

                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Lugar de colecta"]."</td>";
                  
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Código de colectav(Cc)"]."-";
                        echo "".$value["# de extracción (Cl)"]."-";
                        echo "".$value["# de herborizado"]."-";
                        echo "".$value["# de accesión"]."</td>";
                     
                    
                 
                       // echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["departamento"]."</td>";
                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["Pais"]."</td>";

                        echo "<td style='border: 1px solid black;width: 100px;font-size: 10px;'>".$value["departamento"]."</td>";
                  

                    




                     

                       echo "</tr>";
                      }


                 ?>
			</tr>
		</tbody>
	</table>
	</body>

	</html>