<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border">
<div class="col-md-12">
<div class="row">  

	<form id="formulario"  onsubmit="return generar()">
	<div class="col-md-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Fecha Inicial</label>
				<input value="" type="date" id="fecha_inicio" name="fecha_inicio" class="form-control">
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<label>Fecha Final</label>
				<input value="" type="date" id="fecha_final" name="fecha_final" class="form-control">
			</div>
		</div>
		<div class="col-md-3" style="margin-top: 25px;">
			<button class="btn btn-primary">Generar</button>
		</div>
	</div>

</form>

</div>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<div id="contenedor1">
					
				</div>
			</div>
				<div class="col-md-4">
				<div id="contenedor2">
					
				</div>
			</div>
				<div class="col-md-4">
				<div id="contenedor3">
					
				</div>
			</div>
            <div class="col-md-4">
                <div id="contenedor4">
                    
                </div>
            </div>
            <div class="col-md-4">
                <div id="contenedor5">
                    
                </div>
            </div>

               <div class="col-md-4">
                <div id="contenedor6">
                    
                </div>
            </div>
		</div>
	</div>
</div>
</div>

</div>
</div>
</div>
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script type="text/javascript">
	function generar(){
 $.post(base_url+"Grafico_reporte/reporte_especie",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico1(response);
               
        },"json");
    

           $.post(base_url+"Grafico_reporte/reporte_genero",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico2(response);
               
        },"json");

           $.post(base_url+"Grafico_reporte/reporte_subtribu",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico3(response);
               
        },"json");

               $.post(base_url+"Grafico_reporte/reporte_tribu",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico4(response);
               
        },"json");

                   $.post(base_url+"Grafico_reporte/reporte_familia",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico5(response);
               
        },"json");


                   $.post(base_url+"Grafico_reporte/reporte_estado_check",$("#formulario").serialize(),function(response){

         //  console.log(response);
           console.log(response);
             cargar_grafico6(response);
               
        },"json");
        // cargar_grafico3();


return false;
    }
	$(function () {


       generar();
	});

        function cargar_grafico1(response){
 Highcharts.setOptions({
     colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4']
    }); 
        Highcharts.chart('contenedor1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICO POR ESPECIE'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: response
    }]
});
    }


	function cargar_grafico2(response){
 Highcharts.setOptions({
     colors: [ '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4','#50B432', '#ED561B', '#DDDF00']
    }); 
		Highcharts.chart('contenedor2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICO POR GÉNERO'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: response
    }]
});
	}
		function cargar_grafico3(response){

		Highcharts.chart('contenedor3', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICO POR SUBTRIBU'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: response
    }]
});
	}

        function cargar_grafico4(response){

        Highcharts.chart('contenedor4', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICO POR TRIBU'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: response
    }]
});
    }

        function cargar_grafico5(response){

        Highcharts.chart('contenedor5', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'GRÁFICO POR FAMILIA'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: response
    }]
});
    }

        function cargar_grafico6(response){
            Highcharts.chart('contenedor6', {

             
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'GRÁFICO DE CLASIFICACIÓN'
                },
                subtitle: {
                 
                },
                xAxis: {
                     allowDecimals: false,
                    categories: [
                        'Estado',
                    
                    ],
                    crosshair: true
                },
                yAxis: {
                     allowDecimals: false,
                    min: 0,
                    title: {
                        text: '(Unidad)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} Unidad</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: response
            });
    }
</script>