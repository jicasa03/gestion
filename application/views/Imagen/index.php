﻿
<style type="text/css">
	
	*{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}



$bootstrap-sm: 576px;
$bootstrap-md: 768px;
$bootstrap-lg: 992px;
$bootstrap-xl: 1200px;


#gallery {
  
  img {
    height: 40vw;
    object-fit: cover;
    
    @media (min-width: $bootstrap-md) {
      height: 25vw;
    }
    
    @media (min-width: $bootstrap-lg) {
      height: 18vw;
    }
  }
}


 
.carousel-item {
  
  img {
    height: 60vw;
    object-fit: cover;
    
    @media (min-width: $bootstrap-sm) {
      height: 350px;
    }
  }
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet" type="text/css" media="screen" />

<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border">


<div class="row" id="gallery" data-toggle="modal" data-target="#exampleModal">

<?php foreach ($data["lista"] as $key => $value) { ?>

  <div class="col-6 col-md-4 col-lg-3">
    <img onclick="mostrar_foto1('<?php echo $value['foto_descripcion'];?>','<?php echo base_url(); ?>public/imagenes/<?php echo $value['foto_nombre'];?>?>')" class="w-100" style="width: 150px;height: 200px;"   
    src="<?php echo base_url(); ?>public/imagenes/<?php echo $value['foto_nombre'];?>" 
    onclick=""
     alt="First slide" data-target="#carouselExample" data-slide-to="0">
  </div>
 <?php } ?>
</div>
</div>
</div>
</div>
<!-- Modal -->
<!-- 
This part is straight out of Bootstrap docs. Just a carousel inside a modal.
-->









	<!--<div class="superbox">

		<?php foreach ($data["lista"] as $key => $value) {?>
		
		
<div class="superbox-list">

 <img src="<?php echo base_url(); ?>public/imagenes/<?php echo $value['foto_nombre'];?>" 
 data-img="<?php echo base_url(); ?>public/imagenes/<?php echo $value['foto_nombre'];?>"
 alt="" class="superbox-img">


  </div>
<?php } ?>
<div class="superbox-float"></div>
</div>	-->
	</div>
</div>

<?php 


   $sql1="select * from perfil where per_id=".$_SESSION["id_perfil"];
 $perfiles=$this->db->query($sql1)->row_array();
    //print_r($perfiles);exit();
  //  $data["perfil"]=$perfiles;

//print_r($data);
$per =$perfiles;
if($per["per_estado_escritura"]=="1"){
  ?>


<a onclick="mostrar_foto()" href="#" class="float">
<i class="fa fa-plus my-float"></i>
</a>
<?php 
   }
?>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
          <div id="carouselExample" class="carousel slide" data-ride="carousel">
        
          <div class="carousel-inner">
            <div class="carousel-item text-center">
              <img id="imagen_datos" class="d-block w-100" style="width: 500px;height: 350px;"   alt="First slide">
            </div>
         
          </div>
       
        </div>
       </div>
       <div class="row">
         <p id="mostrar_descripcion" class="text-center">
          Esta foto se muestra siempre  
         </p>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>








<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>


<h4 id="myModalLabel" class="semi-bold">REGISTRAR FOTO</h4>


</div>
<form id="formulario_agenda" onsubmit="return crear_agenda()">
<div class="modal-body">
	<div class="row">
		<div class="col-md-3"  style="padding-top: 10px;">
			<label style="padding-top: 7px;">DESCRIPCIÓN :</label>
		</div>
		<div class="col-md-9">
			<input type="text" class="form-control" required="true" autocomplete="off" name="descripcion" id="descripcion">
		</div>
	</div>
		<div class="row"  style="padding-top: 10px;">
		<div class="col-md-3" >
			<label style="padding-top: 7px;">IMAGEN :</label>
		</div>
		<div class="col-md-9">
			<input type="file" required="true"  autocomplete="off" name="subir_foto" id="subir_foto">
		</div>
	</div>


</div>
 <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		<button type="submit" class="btn btn-primary" id="btn_notificar">Guardar</button>
</div>

</form>
</div>

</div>

</div>











<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js" type="text/javascript"></script>






<script type="text/javascript">

function mostrar_foto1(descripcion,foto){

    $("#imagen_datos").attr("src",foto);
    $("#mostrar_descripcion").text(descripcion);

}


    
      // Call SuperBox - that's it!
     // $('.superbox').SuperBox();

	function crear_agenda(){

		$("#btn_notificar").attr("disabled",true);
		$("#btn_notificar").text("Registrando..");



var form = $('#formulario_agenda')[0]; // You need to use standard javascript object here
var formData = new FormData(form);

	$.ajax({
			    url: base_url+'Imagen/guardar',
			    data: formData,
			    type: 'POST',
			    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
			    processData: false, 
                     type: "POST",
                      dataType: "json",
		  			 success : function(data){
		          

		            if(data["estado"]==1){

 								mostrar_notificacion_success(data["mensaje"]);
 								 //window.location=base_url+""
 								

 								 setTimeout(function(){ location.reload(); }, 1000);
                             
		            }
		            if(data["estado"]==2){

 								mostrar_notificacion_success(data["mensaje"]);
 								 //window.location=base_url+""
 								

 								 setTimeout(function(){   window.location=base_url+"Registro";}, 1000);
                             
		            }


		            if(data["estado"]==0){


                       mostrar_notificacion_error(data["mensaje"]);
		            }



					  $("#btn_notificar").attr("disabled",false);
					$("#btn_notificar").text("Guardar");
		            }
		});




            /* $.post(base_url+"Dashboard/guardar_agenda",$("#formulario_agenda").serialize(),function(response){
                    console.log(response);
                  if(response["estado"]==1){
                     mostrar_notificacion_success("Se registro correctamente");
                      $("#formulario_agenda")[0].reset();
                    $("#myModal").modal("hide");
                  }else{


                  	mostrar_notificacion_error("SE GENERO UN ERROR");
                  }
                   $("#btn_notificar").attr("disabled",false);
		$("#btn_notificar").text("Notificar");

             },"json");*/
		return false;
	}

function mostrar_foto(){
               $("#formulario_agenda")[0].reset();
           $("#myModal").modal();


 }


</script>