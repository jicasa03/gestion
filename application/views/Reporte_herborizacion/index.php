<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border"><div class="row">  

	<form id="formulario"  onsubmit="return generar_pdf()">
	<div class="col-md-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Fecha Inicial</label>
				<input value="<?php  echo  date("Y-m-d") ?>" type="date" id="fecha_inicio" name="fecha_inicio" class="form-control">
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<label>Fecha Final</label>
				<input value="<?php  echo  date("Y-m-d") ?>" type="date" id="fecha_final" name="fecha_final" class="form-control">
			</div>
		</div>
		<div class="col-md-3" style="margin-top: 25px;">
			<button class="btn btn-primary">Generar</button>
		</div>
	</div>
	<div class="col-md-12">
		<div class="row"  style="padding-top: 20px;">
			<div class="col-md-3">
				<diV class="radio">
					<input id="reporte_g" type="radio" name="reporte" value="1" checked="checked">
					<label for="reporte_g">R. por Género y Especie </label>
		      </diV>
			</div>
				<div class="col-md-3">
					<diV class="radio">
			<input id="reporte_e" type="radio" name="reporte" value="2">
			<label for="reporte_e">R. Congelación/Ambiente</label>
		</diV>
</div>
	<div class="col-md-3">
		<diV class="radio">
			<input id="female" type="radio" name="reporte" value="3">
			<label for="female">R. por Lugar colecta</label>
		</diV>
</div>
	<div class="col-md-3">
		<diV class="radio">
			<input id="reporte_es" type="radio" name="reporte" value="4">
			<label for="reporte_es">R. por Estado</label>
		</diV>
</div>



</div>

<div class="row" style="padding-top: 20px;">
	<div class="col-md-3">
		<diV class="radio">
			<input id="reporte_c" type="radio" name="reporte" value="5">
			<label for="reporte_c">R. por Hoja Seca</label>
		</diV>
</div>
	<div class="col-md-3">
		<diV class="radio">
			<input id="reporte_f" type="radio" name="reporte" value="6">
			<label for="reporte_f">R. por Polvo</label>
		</diV>
		</div>

		<div class="col-md-3">
		<diV class="radio">
			<input id="reporte_adn" type="radio" name="reporte" value="7">
			<label for="reporte_adn">R. por Extractos ADN</label>
		</diV>
		</div>

		<div class="col-md-3">
		<diV class="radio">
			<input id="reporte_h" type="radio" name="reporte" value="8">
			<label for="reporte_h">R. por Herborización</label>
		</diV>
		</div>


</div>
	</div>

</form>
</div>
</div>
</div>
</div>
<div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
                    <form id="form_permiso" name="form_permiso" onsubmit="return guardar_permiso()">  
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <br>
           
            <h4 id="myModalLabel" class="semi-bold">REPORTE</h4>
            
            <br>
            </div>
            <div class="modal-body">
				<div class="text-center">
					<iframe id="cargar_pdf" type="application/pdf" src="" height="500" width="800"></iframe>
				</div>
           </div>
             <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary" id="boton_permiso">Guardar Cambio</button>
            </div>

               </form>
              
        </div>

        </div>

</div>


<script type="text/javascript">
	function generar_pdf() {
		
                   $.post(base_url+"Reporte_herborizacion/generar_pdf",$("#formulario").serialize(),function(response){
                       console.log(response);
                         var iframe = document.getElementById("cargar_pdf");

           					 iframe.setAttribute("src", base_url+response["pdf"]);
                       $("#myModal").modal();
                       //$("#cargar_pdf").attr("src",base_url+response["pdf"]);
                      

                   },
                   "json");
		return false;
	}
</script>