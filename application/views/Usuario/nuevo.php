
<?php


 ?>
<div class="grid simple ">
<div class="grid-title no-border">

<form onsubmit="return guardar_info()" id="formulario" name="formulario" >
	<input type="hidden" id="id" name="id" value="">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Nombre Completo
			</label>
			<input type="text" required="true" autocomplete="off" name="nombre_completo" id="nombre_completo" class="form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Usuario
			</label>
			<input type="text" required="true" autocomplete="off" name="usuario" id="usuario" class="form-control">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>
				Clave
			</label>
			<input type="text" required="true" autocomplete="off" name="clave" id="clave" class="form-control">
		</div>
	</div>
		<div class="col-md-4">
		<div class="form-group">
			<label>
				Perfil
			</label>
			<select required="true" class="form-control" id="perfil" name="perfil">
				<option value="">SELECCIONAR</option>
				<?php 
                         foreach ($data["perfiles"] as $key => $value) {
                         	echo "<option value='".$value["per_id"]."'>".$value["per_descripcion"]."</option>";
                         }

				?>
			</select>
		</div>
	</div>
</div>

<div class="row">
	<center>
		<a href="<?php echo base_url(); ?>Usuario"><button type="button" class="btn btn-default">CANCELAR</button></a> <button  id="button_guardar" type="submit" class="btn btn-primary">GUARDAR</button>
	</center>
</div>

</form>
</div>
</div>
<script type="text/javascript">
<?php 
if(isset($data["id"])){ ?>

$(function(){

	$.post(base_url+"Usuario/mostrar",{"id":"<?php echo $data["id"] ;?>"},function(data){
            $("#usuario").attr("readonly",true)
		$("#id").val(data[0]["usu_id"]);
	$("#nombre_completo").val(data[0]["usu_nombre_completo"]);
		$("#usuario").val(data[0]["usu_usuario"]);
			$("#clave").val(data[0]["usu_clave"]);
				$("#perfil").val(data[0]["usu_perfil"]);
	},"json");
});

<?php } ?>
	function guardar_info() {

  $("#button_guardar").text("Guardando...");
                $("#button_guardar").attr("disabled",true);
		 $.post(base_url+"Usuario/guardar",$("#formulario").serialize(),function(data){

		 	  if(data["estado"]){
               window.location=base_url+"Usuario";
		 	  }else{

		 	  	toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"](data["Mensaje"]);


                $("#button_guardar").text("GUARDAR");
                $("#button_guardar").attr("disabled",false);

		 	  }

		 },"json");

		return false;
	}
</script>