<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border">


<form onsubmit="return guardar_info()" id="formulario" name="formulario" >
	<input type="hidden" id="id" name="id" value="">
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<label>
				NOMBRE COMPLETO
			</label>
			<input type="text" required="true" autocomplete="off" name="nombre_completo" id="nombre_completo" class="form-control">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label>
				CLAVE
			</label>
			<input type="text" required="true" autocomplete="off" name="clave" id="clave" class="form-control">
		</div>
	</div>
		<div class="col-md-4">
		<div class="form-group">
			<label>Foto de Perfil</label>
			<input type="file" name="subir_foto" id="subir_foto" >
		</div>
	</div>
</div>

<div class="row">
	<center>
	 <button  id="button_guardar" type="submit" class="btn btn-primary">GUARDAR</button>
	</center>
</div>

</form>
</div>
</div>
</div>
<script type="text/javascript">
<?php 
if(isset( $_SESSION["id_usuario"])){ ?>

$(function(){

	$.post(base_url+"Usuario/mostrar",{"id":"<?php echo  $_SESSION["id_usuario"] ;?>"},function(data){
            $("#usuario").attr("readonly",true)
		$("#id").val(data[0]["usu_id"]);
	$("#nombre_completo").val(data[0]["usu_nombre_completo"]);
		$("#usuario").val(data[0]["usu_usuario"]);
			$("#clave").val(data[0]["usu_clave"]);
				$("#perfil").val(data[0]["usu_perfil"]);
	},"json");
});

<?php } ?>
	function guardar_info() {

  $("#button_guardar").text("Guardando...");
                $("#button_guardar").attr("disabled",true);


                var form = $("#formulario")[0]; // You need to use standard javascript object here
var formData = new FormData(form);

	$.ajax({
			    url: base_url+'Usuario/actualizar_perfil',
			    data: formData,
			    type: 'POST',
			    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
			    processData: false, 
                     type: "POST",
                      dataType: "json",
		  			 success : function(data){
				          
		                             if(data["estado"]){
		               window.location=base_url+"dashboard";
				 	  }else{

				 	  	toastr.options = {
		                    "closeButton": true,
		                    "debug": false,
		                    "newestOnTop": false,
		                    "progressBar": false,
		                    "positionClass": "toast-bottom-right",
		                    "preventDuplicates": false,
		                    "showDuration": "300",
		                    "hideDuration": "1000",
		                    "timeOut": "5000",
		                    "extendedTimeOut": "1000",
		                    "showEasing": "swing",
		                    "hideEasing": "linear",
		                    "showMethod": "fadeIn",
		                    "hideMethod": "fadeOut"
		                }
		                toastr["error"](data["Mensaje"]);


		                $("#button_guardar").text("GUARDAR");
		                $("#button_guardar").attr("disabled",false);

				 	  }


		            }
		});


		/* $.post(base_url+"Usuario/guardar",$("#formulario").serialize(),function(data){

		 	  if(data["estado"]){
               window.location=base_url+"Usuario";
		 	  }else{

		 	  	toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"](data["Mensaje"]);


                $("#button_guardar").text("GUARDAR");
                $("#button_guardar").attr("disabled",false);

		 	  }

		 },"json");
*/
		return false;
	}
</script>