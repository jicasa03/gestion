﻿<?php 

//print_r( $data["usuario_fuera"]);

?>

<style type="text/css">
	
	*{padding:0;margin:0;}

body{
	font-family:Verdana, Geneva, sans-serif;
	font-size:18px;
	background-color:#CCC;
}

.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}
</style>
<div class="row 2col">

<div  class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
<div class="tiles  added-margin" style="background-color: white !important;border-radius: 10px !important;">
	<i style="color: #2e7d32;padding: 20px;font-size: 80px;" class="fa fa-cloud fa-7x" id="icon-resize"></i>

<div class="tiles-body">


<div style="padding-bottom: 15px;color:black;" class="heading">
 <span class="animate-number" data-value="<?php echo  $data['total_numero']; ?>" data-animation-duration="1200">0</span>
</div>
<div class="tiles-header" style="color:black;font-size: 15px;padding-bottom: 25px;"> 
	N° total de material botánico
 </div>

</div>
</div>
</div>


<div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
<div class="tiles  added-margin" style="background-color: white !important;border-radius: 10px !important;">
	<i style="color: #0097a7;padding: 20px;font-size: 80px;" class="fa fa-signal fa-7x" id="icon-resize"></i>

<div class="tiles-body">
<div class="controller">

</div>

<div class="heading" style="color:black;"> <span class="animate-number" data-value="<?php echo $data['total_numero_mes']; ?>" data-animation-duration="1000">0</span> </div>
<div class="tiles-header" style="color:black;font-size: 15px;padding-bottom: 18px;"> N° total de material botánico por mes</div>
</div>
</div>
</div>



<div class="col-md-3 col-sm-6 spacing-bottom">
<div style="background-color:white;border-radius: 10px !important;" class="tiles  added-margin">
	<i style="color: #ef6c00;padding: 20px;font-size: 80px;" class="fa fa-user fa-7x" id="icon-resize"></i>

<div class="tiles-body">
<div class="controller">

</div>

<div style="padding-bottom: 15px;color:black;"  class="heading"> <span class="animate-number" data-value="<?php echo $data["usuario"]; ?>" data-animation-duration="1200">0</span> </div>
<div class="tiles-header" style="color:black;font-size: 15px;padding-bottom: 3px;">N° total de usuarios del sistema </div>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div style="background-color: white;border-radius: 10px !important;" class="tiles  added-margin">
	<i style="color: #006064;padding: 20px;font-size: 80px;" class="fa fa-cloud-upload fa-7x" id="icon-resize"></i>

<div class="tiles-body">

<div class="controller">

</div>




<div class="row-fluid">
<div style="color:black;" class="heading"> <span class="animate-number" data-value="<?php echo $data["usuario_fuera"][0]["contador"]; ?>"  data-animation-duration="700">0</span> </div>
<div style="color:black;font-size: 15px;padding-bottom: 18px;" class="tiles-header">N° de personas que no se encuentran en el programa </div>
</div>

</div>
</div>
</div>
</div>
</div>







<!--<div class="row">
	
	<div class="col-md-12">

<div id="myCarousel" class="carousel slide" data-ride="carousel">
 

  <ol class="carousel-indicators">

  	<?php foreach ($data["carousel"] as $key => $value) {?>
 

    <li data-target="#myCarousel" data-slide-to="<?php echo $key ?>" <?php if($key==0){echo 'class="active"';} ?>></li>
<?php } ?>
  </ol>

  <div class="carousel-inner">

  	<?php foreach ($data["carousel"] as $key => $value) {?>
  	
  	
    <div class="item <?php if($key==0){echo 'active';} ?>">
      <img style="height: 400px;width: 100%;" src="<?php  echo base_url()?>public/carrousel/<?php echo $value['carousel_nombre']; ?>"  alt="Los Angeles">
    </div>
<?php } ?>
   
  </div>


  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
		
	</div>
</div>-->

<?php 


   $sql1="select * from perfil where per_id=".$_SESSION["id_perfil"];
 $perfiles=$this->db->query($sql1)->row_array();
    //print_r($perfiles);exit();
  //  $data["perfil"]=$perfiles;

//print_r($data);
$per =$perfiles;
if($per["per_estado_escritura"]=="1"){

?>


<a onclick="mostrar_fecha()" href="#" class="float">
<i class="fa fa-plus my-float"></i>
</a>


<?php 
  }
?>

<div style="margin-top: 100px;" class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>


<h4 id="myModalLabel" class="semi-bold">REGISTRAR AGENDA</h4>


</div>
<form id="formulario_agenda" onsubmit="return crear_agenda()">
<div class="modal-body">
	<div class="row">
		<div class="col-md-3"  style="padding-top: 10px;">
			<label style="padding-top: 7px;">AGENDA :</label>
		</div>
		<div class="col-md-9">
			<input type="text" class="form-control" required="true" autocomplete="off" name="descripcion" id="descripcion">
		</div>
	</div>
		<div class="row"  style="padding-top: 10px;">
		<div class="col-md-3" >
			<label style="padding-top: 7px;">LUGAR :</label>
		</div>
		<div class="col-md-9">
			<input type="text" class="form-control" required="true"  autocomplete="off" name="lugar" id="lugar">
		</div>
	</div>
		<div class="row" style="padding-top: 10px;">
		<div class="col-md-3" >
			<label style="padding-top: 7px;">FECHA DE IDA :</label>
		</div>
		<div class="col-md-9">
			<input type="date" class="form-control" min="<?php  echo date("Y-m-d");?>" required="true" autocomplete="off" name="fecha" id="fecha">
		</div>
	</div>	

	<div class="row" style="padding-top: 10px;">
		<div class="col-md-3" >
			<label style="padding-top: 7px;">FECHA DE RETORNO :</label>
		</div>
		<div class="col-md-9">
			<input type="date" class="form-control" min="<?php  echo date("Y-m-d");?>" required="true" autocomplete="off" name="fecha_regreso" id="fecha_regreso">
		</div>
	</div>	
</div>
 <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		<button type="submit" class="btn btn-primary" id="btn_notificar">Notificar</button>
</div>

</form>
</div>

</div>

</div>
<script type="text/javascript">

	
	function crear_agenda(){

		$("#btn_notificar").attr("disabled",true);
		$("#btn_notificar").text("Registrando..");

             $.post(base_url+"Dashboard/guardar_agenda",$("#formulario_agenda").serialize(),function(response){
                    console.log(response);
                  if(response["estado"]==1){
                     mostrar_notificacion_success("Se registró correctamente");
                      $("#formulario_agenda")[0].reset();
                    $("#myModal").modal("hide");
                  }else{


                  	mostrar_notificacion_error("Se generó un error");
                  }
                   $("#btn_notificar").attr("disabled",false);
		$("#btn_notificar").text("Notificar");

             },"json");
		return false;
	}

function mostrar_fecha(){
               $("#formulario_agenda")[0].reset();
           $("#myModal").modal();


 }


</script>