﻿<!DOCTYPE html>
<html>

<!-- Mirrored from webarch.revox.io/3.0/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Sep 2019 14:00:42 GMT -->
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Sistema de Información de Gestión de Material Botánico</title>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/img/logo.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta content="" name="description" />
<meta content="" name="author" />


<link href="<?php echo base_url();?>public/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo base_url();?>public/assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>public/img/logo.png">
<link href="<?php echo base_url();?>public/assets/plugins/jquery-notifications/css/messenger.css" rel="stylesheet" type="text/css" media="screen" />

<link href="<?php echo base_url();?>public/webarch/css/webarch.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>public/libreria/toast/toastr.min.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Ubuntu" />
<link rel="stylesheet" type="text/css" href="<?php echo  base_url()?>public/icomoon/styles.css" />




</head>
<style type="text/css">
	.lds-ring {
  display: inline-block;
  position: relative;
  width: 40px;
  height: 45px;
}
.lds-ring div {
  box-sizing: border-box;
  display: block;
  position: absolute;
  width: 30px;
  height: 30px;
  margin: 5px;
  border:2px solid black;
  border-radius: 50%;
  animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
  border-color: black transparent transparent transparent;
}
.lds-ring div:nth-child(1) {
  animation-delay: -0.45s;
}
.lds-ring div:nth-child(2) {
  animation-delay: -0.3s;
}
.lds-ring div:nth-child(3) {
  animation-delay: -0.15s;
}
@keyframes lds-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

	body {
 
 background-image: url("public/img/fondo.JPG");
 -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  background-color: rgba(0, 0, 0, 0.5);
}

.card {
  /* Add shadows to create the "card" effect */
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

#fondo_negro{

position:absolute;
	padding: 0px;
	width: 100%;
	height: 100%;
	margin:0px;
	background-color: black;
	opacity: 0.5;
}

.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
  background-color: #333 !important;
}

.column-seperation{
 

}

.btn-circle.btn-xl {
    width: 70px;
    height: 70px;
    padding: 10px 16px;
    border-radius: 35px;
    font-size: 24px;
    line-height: 1.33;
}

.btn-circle {
    width: 50px;
    height: 50px;
    padding: 6px 0px;
    border-radius: 25px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
}

.group 			  { 
  position:relative; 
  margin-bottom:45px; 
}
input 				{
  font-size:10px;
  padding:10px 10px 10px 5px;
  display:block;
  width:150px;
  border:none;
  border-bottom:1px solid #757575;
}
input:focus 		{ outline:none; }

/* LABEL ======================================= */
label 				 {
  color:#999; 
  font-size:16px;
  font-weight:normal;
  position:absolute;
  pointer-events:none;
  left:5px;
  top:10px;
  transition:0.2s ease all; 
  -moz-transition:0.2s ease all; 
  -webkit-transition:0.2s ease all;
  font-family: Ubuntu;
}

/* active state */
input:focus ~ label, input:valid ~ label 		{
  top:-20px;
  font-size:14px;
  color:#5264AE;
}

/* BOTTOM BARS ================================= */
.bar 	{ position:relative; display:block; width:150px; }
.bar:before, .bar:after 	{
  content:'';
  height:2px; 
  width:0;
  bottom:1px; 
  position:absolute;
  background:#5264AE; 
  transition:0.2s ease all; 
  -moz-transition:0.2s ease all; 
  -webkit-transition:0.2s ease all;
}
.bar:before {
  left:50%;
}
.bar:after {
  right:50%; 
}

/* active state */
input:focus ~ .bar:before, input:focus ~ .bar:after {
  width:50%;
}

/* HIGHLIGHTER ================================== */
.highlight {
  position:absolute;
  height:60%; 
  width:100px; 
  top:25%; 
  left:0;
  pointer-events:none;
  opacity:0.5;
}

/* active state */
input:focus ~ .highlight {
  -webkit-animation:inputHighlighter 0.3s ease;
  -moz-animation:inputHighlighter 0.3s ease;
  animation:inputHighlighter 0.3s ease;
}

/* ANIMATIONS ================ */
@-webkit-keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}
@-moz-keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}
@keyframes inputHighlighter {
	from { background:#5264AE; }
  to 	{ width:0; background:transparent; }
}

.border-slate-300 {
    border-color: #90A4AE;
}

.text-slate-300, .text-slate-300:hover, .text-slate-300:focus {
    color: #90A4AE !important;
}

#header_arriba{
  width: 100%;
  height: 50px;
  background-color:#6f7b8a;
}
</style>

<body class="error-body no-top">

<div id="fondo_negro">
</div>
 <div id="header_arriba">
      <label style="margin-left:400px;padding-top: 10px;font-size: 25px;color:white;margin-top: 0px;font-family: Ubuntu;">Sistema de Información de Gestión de Material Botánico</label>
</div>



	<div  class="col-md-12 " style="margin-top: 10px;width: 100% !important  ;">



	<div class="row " style="width: 100% !important;">
    <div class="col-md-3">
      
    </div>
		
		<div class="col-md-9 text-center">
	
		</div>
	</div>

</div>






<div class="col-md-12 " style="" >



<div class="col-md-4">
  
</div>


<div style="margin-left: 80px;" class="col-md-3 text-center" style="margin-right: 0px;margin-top: 0px;">

	<div  class="card w3-panel" style=" border-radius: 3px;background-color: #ffffff;padding-bottom: 10px;margin-top: 80px;padding-left: 40px;padding-right: 30px;">
  
  <div  class="card-body">


<br>
<form  onsubmit="return loguear()" class="login-form " id="login-form" method="post" name="login-form">

	<div class="text-center">

								<div class="icon-object border-slate-300 text-slate-300"><i style="font-size: 50px;" class="icon-reading"></i></div>

							
        							</div>
        	<div class="row"> 
        		 <h3 class="text-center" style="font-family: Ubuntu;">
        		 	Iniciar Sesión
        		 </h3>

        	</div>





          

            <div class="row">
            	<div style="margin-top: 10px;margin-right: 10px;" class="col-md-1">
            		<i style="margin-left: 15px;margin-top: 5px;" class="material-icons">
                  account_circle
                 </i>
            	</div>
               <div class="form-group col-md-8 "  style="margin-bottom: 0px !important;">


              <div style="margin-top: 10px;" class="group">      
                <input style="border:none !important;"  id="usuario" name="usuario" autocomplete="off" type="text" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label>Usuario</label>
              </div>
<!--<input class="form-control" id="usuario" name="usuario" type="text" autocomplete="off" required>-->

                </div>
                </div>
                <div class="row" style="height: 15px !important;">
                	<div class="col-md-1" style="margin-top: 10px;margin-right: 10px;">
                		<i style="margin-left: 15px;margin-top: 0px;" class="material-icons">
                security
                </i>
                	</div>
                    <div class="form-group col-md-4" style="height: 30px !important;">
                     <div style="margin-top: 0px;" class="group">      
                          <input style="border:none !important;"  id="clave" name="clave" autocomplete="off" type="password" required>
                          <span class="highlight"></span>
                          <span class="bar"></span>
                          <label>Clave</label>
                        </div>
      <!--<input class="form-control" id="clave" name="clave" type="password" autocomplete="off" required>-->
                </div>
          </div>
     
                 <div class="col-md-12"  style="height: 40px; ">
                              <input style="margin-right: 50px;margin-top: 15px;" id="checkbox1" type="checkbox" value="1">
                  <label style="font-size: 13px;padding-left: 95px;padding-top: 30px;
 font-family: Ubuntu;color: #505458;" >
         
                     Guardar Sesión
                     </label>
              </div>
     
       
        
  
      

















<div class="row">
<div class="col-md-8 text-center">


<button class="btn btn-primary pull-right" id="boton_login" type="submit" style="background-color: #333 !important;font-family: Ubuntu;">
Ingresar	

</button>
</div>
</div>
</form>
</div>
</div>
</div>

</div>



<div class="col-md-5">
        <img   src="<?php echo base_url() ?>public/img/logo1.png"  style="height: 80px;margin-left: 655px;margin-top: 20px;">
      </div>

<!--

<div class="col-md-12">

<div class="row " style="margin-top: 0px;">

<h2 style="margin-left:125px;color:white;font-family: Ubuntu;font-size: 27px;">
Instituto de Investigaciones de la Amazonía Peruana
</h2>
<p style="margin-left:125px;color:white;margin-top: 30px;font-size: 20px;font-family: Ubuntu;">
Laboratorio de Botánica Aplicada Jean-Christophe Pintaud (LBA-JCP)
<br>

</p>
<br>

</div>




</div>
-->



















<div class="footer">
 <p style="color:#333">-</p>
</div>
</body>

<script src="<?php echo base_url();?>public/assets/plugins/pace/pace.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>public/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/assets/plugins/bootstrap-select2/select2.min.js" type="text/javascript"></script>



<script src="<?php echo base_url()?>public/webarch/js/webarch.js" type="text/javascript"></script>



<script src="<?php echo base_url()?>public/libreria/toast/toastr.min.js" type="text/javascript"></script>

<script type="text/javascript">

	$(function(){
		//$("#cargar").hide();
	});
	base_url="<?php echo base_url(); ?>";
	function loguear() {
		if($("#usuario").val()!="" && $("#clave").val()!=""){
		//$("#boton_login").text("Cargando...");
		$("#boton_login").attr("disabled",true);
		$("#boton_login").removeClass("btn-primary");
		$("#boton_login").addClass("btn-circle");
			$("#boton_login").text("");
                 $("#boton_login").html('<div id="cargar" class="lds-ring"><div></div><div></div><div></div><div></div></div>');
           $.post(base_url+"Login/validar_clave", $("#login-form").serialize(),function(data){

    if(data["estado"]){
  window.location=base_url+"dashboard";

    }else{
 	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-bottom-right",
			  "preventDuplicates": false,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
			toastr["error"](data["mensaje"]);

               
                //  $("#boton_login").text("Iniciar");
					$("#boton_login").attr("disabled",false);

$("#boton_login").addClass("btn-primary");
		$("#boton_login").removeClass("btn-circle");
		  $("#boton_login").html('');
			$("#boton_login").text("Ingresar");
               


    }
          
			
           },"json");
		}
		return false;

	}
</script>


<!-- Mirrored from webarch.revox.io/3.0/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 04 Sep 2019 14:00:42 GMT -->
</html>
