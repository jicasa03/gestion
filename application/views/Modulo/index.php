<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border">


	<button id="test2" class="btn btn-primary">NUEVO MÓDULO</button>

<br>
<br>
<div class="row">
	<div class="col-md-12">
		<table class="table" id="example3">
			<thead>
			<tr>
			<th>#</th>
			<th>Módulo</th>
			<th>Url</th>
			<th>Ícono</th>
			<th>Acción</th>
			</tr>
			</thead>
			<tbody>

			<?php
			 foreach ($data["lista"] as $key => $value) {
			 	$a=$value["mod_id"].",'".$value["mod_descripcion"]."','".$value["mod_url"]."','".$value["mod_icono"]."',".$value["mod_padre"];
			 	echo '<tr class="odd gradeX">';
			echo '<td>'.($key+1).'</td>';
			echo '<td>'.$value["mod_descripcion"].'</td>';
			echo '<td>'.$value["mod_url"].'</td>';
			echo '<td class="center"> '.$value["mod_icono"].'</td>';
			echo '<td class="center">
<a title="Editar" onclick="editar('.$a.')" class="btn btn-primary"><i class="fa fa-paste "></i> </a> 
<a title="Eliminar" class="btn btn-danger" onclick="eliminar('.$value["mod_id"].')">
<i class="fa fa-trash "></i></a></td>';
			echo '</tr>';
			 }



			?>



			</tbody>
		</table>
	</div>
</div>



<div class="admin-bar" id="quick-access" style="display:">
<div class="admin-bar-inner">

<form  id="form_modulo" onsubmit="return guardar_info()">
<div class="form-horizontal">
	<input type="hidden" id="id" name="id">
	<div class="col-md-3 col-sm-3 ">
	<input  type="text" class="form-control" autocomplete="off" placeholder="Nombre de Módulo" id="modulo" name="modulo" required="true">
</div>
<div class="col-md-3 col-sm-3 ">
	<input  type="text" class="form-control" autocomplete="off" placeholder="Nombre del URL" name="url" id="url" required="true">
</div>
<div class="col-md-3 col-sm-3 ">
	<input  type="text" class="form-control" autocomplete="off" placeholder="Nombre de Icono" name="icono" id="icono">
</div>
<select id="padre" name="padre" required="true">
<option value="">Seleccionar</option>
<option value="0">Es Modulo Padre</option>
<?php
foreach ($data["padres"]  as $key => $value) {
	echo '<option value="'.$value["mod_id"].'">'.$value["mod_descripcion"].'</option>';
}

?>
</select>
</div>
<button class="btn btn-primary btn-cons btn-add" id="boton_agregar_modulo" type="submit">AGREGAR</button>
<button class="btn btn-white btn-cons btn-cancel" type="button" onclick='$("#quick-access").css("bottom","-115px");'>Cancelar</button>
</div>
</form>


</div>



</div>
</div>
</div>
<script type="text/javascript">
	function editar(id,descripcion,url,icono,padre) {

        $("#quick-access").css("bottom","0px");
       $("#id").val(id);
        $("#modulo").val(descripcion);
        $("#url").val(url);
        $("#icono").val(icono);
        $("#padre").val(padre);


    }

function eliminar(id){
    $("#quick-access").css("bottom","-115px");
    $("#id_eliminar").val(id);
  $("#modal_eliminar").modal({
  keyboard: false,
      backdrop:'static',

	
});


}

function eliminar_datos(){
    $("#modal_boton_eliminar").text("Eliminando...");
    $("#modal_boton_eliminar").attr("disable",true);
    $.post(base_url+"Modulo/delete_modulo",{"id":$("#id_eliminar").val()},function (data) {
		console.log(data);
		if(data["estado"]){
            location.reload();
		}else{
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("ERROR AL REGISTRAR");


            $("#boton_agregar_modulo").text("AGREGAR MODULO");
            $("#boton_agregar_modulo").attr("disabled",false);
		}
    },"json");

}

	function guardar_info(){
     $("#boton_agregar_modulo").text("Guardando...");
$("#boton_agregar_modulo").attr("disabled",true);
     $.post(base_url+"Modulo/guardar_modulo",$("#form_modulo").serialize(),function(data){
           if(data["estado"]){
                   location.reload();
           }
           else{
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": false,
			  "positionClass": "toast-bottom-right",
			  "preventDuplicates": false,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
			toastr["error"]("ERROR AL REGISTRAR");


                  $("#boton_agregar_modulo").text("AGREGAR MODULO");
					$("#boton_agregar_modulo").attr("disabled",false);

           }
     },"json");
       return false;

	}
$(document).ready(function() {
	$('#test2').on( "click",function() {

        $('#form_modulo')[0].reset();
		$("#quick-access").css("bottom","0px");
    });
	 var oTable3 = $('#example3').dataTable( {
	   "sDom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
        			"oTableTools": {

		},
         "dom": '<"pull-left"f><"pull-right"l>tip',
      
				"oLanguage":{
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
},
    });
	    });
</script>
