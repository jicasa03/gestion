<div class="grid simple ">
<div class="grid-title no-border">



<div class="grid-body no-border"><button id="test2" class="btn btn-primary">NUEVO GÉNERO</button>
<br>
<br>
<div class="row">
	<div class="col-md-12">
		<table class="table" id="example3">
			<thead>
			<tr>
				<th>#</th>
				<th>Descripcion</th>
				<th>Subtribu</th>

				<th>Accion</th>
			</tr>
			</thead>
			<tbody>
           <?php 
                  foreach ($data["lista"] as $key => $value) {
$a=$value["genero_id"].",'".$value["genero_descripcion"]."',".$value["subtribu_id"];

                  	echo "<tr>";
                     echo "<td>".($key+1)."</td>";
                     echo "<td>".$value["genero_descripcion"]."</td>";
                                  echo "<td>".$value["subtribu_descripcion"]."</td>";
                 	 echo '<td class="center">
                    
<a title="Editar" onclick="editar('.$a.')" class="btn btn-primary"><i class="fa fa-paste "></i> </a> 
<a title="Eliminar" class="btn btn-danger" onclick="eliminar('.$value["genero_id"].')">
<i class="fa fa-trash "></i></a></td>';
                  	echo "</tr>";
                  }

           ?>
			</tbody>
		</table>
	</div>
</div>

<div class="admin-bar" id="quick-access" style="display:">
	<div class="admin-bar-inner">

		<form  id="form_perfil" onsubmit="return guardar_info()">
			<div class="form-horizontal">
				<input type="hidden" id="id" name="id">
                    <div class="col-md-6 col-sm-6">
                    <select class="form-control" id="subtribu_id" name="subtribu_id" >
                        <?php 
                              foreach ($data["subtribu"] as $key => $value) {
                                echo "<option value='".$value["subtribu_id"]."'>".$value["subtribu_descripcion"]."</option>";
                              }

                        ?>
                    </select>
                </div>
				<div class="col-md-6 col-sm-6">
					<input  type="text" class="form-control" autocomplete="off" placeholder="Descripción" id="descripcion" name="descripcion" required="true">
				</div>
			

			</div>
			<button class="btn btn-primary btn-cons btn-add" id="boton_agregar_perfil" type="submit">AGREGAR</button>
			<button class="btn btn-white btn-cons btn-cancel" type="button" onclick='$("#quick-access").css("bottom","-115px");'>Cancelar</button>
		</form>
</div>

</div>
</div>
</div>
</div>
<script type="text/javascript">
	    function eliminar(id){
        $("#quick-access").css("bottom","-115px");
        $("#id_eliminar").val(id);
        $("#modal_eliminar").modal({
            keyboard: false,
            backdrop:'static',


        });


    }


     function eliminar_datos(){
        $("#modal_boton_eliminar").text("Eliminando...");
        $("#modal_boton_eliminar").attr("disable",true);
        $.post(base_url+"Genero/delete",{"id":$("#id_eliminar").val()},function (data) {
            console.log(data);
            if(data["estado"]){
                location.reload();
            }else{
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("ERROR AL REGISTRAR");


                $("#boton_agregar_modulo").text("AGREGAR MODULO");
                $("#boton_agregar_modulo").attr("disabled",false);
            }
        },"json");

    }
 function editar(id,descripcion,subtribu_id){
             $('#form_perfil')[0].reset();
            $("#quick-access").css("bottom","0px");
            $("#id").val(id);
            $("#descripcion").val(descripcion);
            $("#subtribu_id").val(subtribu_id);

         
    }
	function guardar_info() {

                  $("#boton_agregar_perfil").attr("disable",true);
         $("#boton_agregar_perfil").text("Guardando...");

         $.post(base_url+"Genero/guardar",$("#form_perfil").serialize(),function(data){
                  
                
                    if(data["estado"]){
                        location.reload();
                    }else{
                          toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr["error"]("ERROR AL REGISTRAR");

                    }
                  $("#boton_agregar_perfil").attr("disable",false);
                         $("#boton_agregar_perfil").text("Guardar Cambio");

         },"json");
		return false;
	}
	   $(document).ready(function() {
        $('#test2').on( "click",function() {

            $('#form_perfil')[0].reset();
            $("#quick-access").css("bottom","0px");
        });
        var oTable3 = $('#example3').dataTable( {
            "sDom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
            "oTableTools": {

            },
         "dom": '<"pull-left"f><"pull-right"l>tip',
            "oLanguage":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });
</script>